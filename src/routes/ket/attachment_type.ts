import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { AttachmentTypeModel } from '../../models/ket/attachment_type'

const attachmentTypeModel = new AttachmentTypeModel();
export default async function attachmentType(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        try {
          let res_: any = await attachmentTypeModel.select(token);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

}