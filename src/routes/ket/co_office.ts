import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { CoOfficeModel } from '../../models/ket/co_office'

const coOfficeModel = new CoOfficeModel();
export default async function coOffice(fastify: FastifyInstance) {
    const hoscode = process.env.HIS_CODE;
    
    // select
    fastify.get('/selectAll',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await coOfficeModel.select(token, hoscode);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })

    fastify.get('/select/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        try {
          let res_: any = await coOfficeModel.cooffice(token, hcode);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/selectLike/:searchText',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let searchText = req.params.searchText;
        try {
          let res_: any = await coOfficeModel.searchText(token, searchText);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }      
    })


}