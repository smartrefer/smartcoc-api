import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { CauseReferbackModel } from '../../models/ket/cause_referback'

const causeReferbackModel = new CauseReferbackModel();
export default async function causeReferback(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await causeReferbackModel.causeReferback(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })


}