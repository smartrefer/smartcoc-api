import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'

// model
import { HisUniversalHiModel } from '../models/his_universal.model';
import { HisHiModel } from '../models/his_hi.model';
import { HisHomcModel } from '../models/his_homc.model';
import { HisHosxpv3Model } from '../models/his_hosxpv3.model';
import { HisHosxpv3ArjaroModel } from '../models/his_hosxpv3arjaro.model';
import { HisHosxpv4Model } from '../models/his_hosxpv4.model';
import { HisHosxpv4PgModel } from '../models/his_hosxpv4pg.model';
import { HisMbaseModel } from '../models/his_mbase.model';
import { HisPhospModel } from '../models/his_phosp.model';
import { HisHimproHiModel } from '../models/his_himpro.model';
import { HisJhcisHiModel } from '../models/his_jhcis.model';
import { HisUbaseModel } from '../models/his_ubase.model';
import { HisSakonModel } from '../models/his_sakon.model';
import { HisHomcUbonModel } from '../models/his_homc_udon.model';
import { HisSoftConModel } from '../models/his_softcon.model'
import { HisSoftConUdonModel } from '../models/his_softcon_udon.model'
import { HisSsbModel } from '../models/his_ssb.model'
import { HisPanaceaplusModel } from '../models/his_panaceaplus.model'
import { HisHosxpv4PCUModel } from '../models/his_hosxpv4_pcu.model';
import { RerferOutModel } from '../models/ket/referout'
import { RerferBackModel } from '../models/ket/referback'
import { HisEphisHiModel } from '../models/his_ephis.model';


const rerferOutModel = new RerferOutModel();
const rerferBackModel = new RerferBackModel();

export default async function test(fastify: FastifyInstance) {
  
// ห้ามแก้ไข // 
//-----------------BEGIN START-------------------//
const provider = process.env.HIS_PROVIDER;
const hoscode = process.env.HIS_CODE;
const connection = process.env.DB_CLIENT;
let db: any;
let hisModel: any;

switch (connection) {
  case 'mysql2':
    db = fastify.mysql2;
    break;
  case 'mysql':
    db = fastify.mysql;
    break;
  case 'mssql':
    db = fastify.mssql;
    break;
  case 'pg':
    db = fastify.pg;
    break;
  case 'oracledb':
    db = fastify.oracledb;
    break;
  default:
      db = fastify.mysql;
}

switch (provider) {
  case 'ubase':
    hisModel = new HisUbaseModel();
    break;
  case 'himpro':
    hisModel = new HisHimproHiModel();
    break;
  case 'jhcis':
    hisModel = new HisJhcisHiModel();
    break;
  case 'hosxpv3':
    hisModel = new HisHosxpv3Model();
    break;
  case 'hosxpv3arjaro':
    hisModel = new HisHosxpv3ArjaroModel();
    break;
  case 'hosxpv4':
    hisModel = new HisHosxpv4Model();
    break;
  case 'hosxpv4pg':
    hisModel = new HisHosxpv4PgModel();
    break;
  case 'hi':
    hisModel = new HisHiModel();
    break;
  case 'homc':
    hisModel = new HisHomcModel();
    break;
  case 'mbase':
    hisModel = new HisMbaseModel();
    break;
  case 'phosp':
    hisModel = new HisPhospModel();
    break;
  case 'universal':
    hisModel = new HisUniversalHiModel();
    break;
  case 'sakon':
    hisModel = new HisSakonModel();
    break;
  case 'homcudon':
    hisModel = new HisHomcUbonModel();
    break;
  case 'softcon':
    hisModel = new HisSoftConModel();
    break;
  case 'softconudon':
    hisModel = new HisSoftConUdonModel();
    break;
  case 'ssb':
    hisModel = new HisSsbModel();
    break;
  case 'panaceaplus':
    hisModel = new HisPanaceaplusModel();
    break;
  case 'hosxppcuv4':
    hisModel = new HisHosxpv4PCUModel();
    break;
    case 'ephis':
      hisModel = new HisEphisHiModel();
      break;
default:
  // hisModel = new HisModel();
}
//------------------BEGIN END------------------//

  fastify.get('/db', async (request: FastifyRequest, reply: FastifyReply) => {
    // const db: any = fastify.knex    
    try {
      const rs: any = await hisModel.getDepartment(db)
      reply.send(rs)
    } catch (error:any) {
      reply.send({ message: error.message })
    }
  })

}
