import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class RerferOutModel {
    async rfout_select_cid(token:any, cid: any,hcode: any,refer: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/selectCid/${cid}/${hcode}/${refer}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfout_select(token:any, hcode: any, sdate: any, edate: any, limit: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/select/${hcode}/${sdate}/${edate}/${limit}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfout_select_reply(token:any, hcode: any, sdate: any, edate: any, limit: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/select_reply/${hcode}/${sdate}/${edate}/${limit}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });

    }

    async rfout_selectOne(token:any, refer_no: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/selectOne/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfout_selectLimit(token:any, limit: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/selectLimit/${limit}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfInsert(token:any, rows: any) {
        // console.log(rows);

        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'POST',
                url: `${urlApi}/referout/insert`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                body: { rows: rows }
                ,
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfUpdate(token:any, refer_no: any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'PUT',
                url: `${urlApi}/referout/update/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                body: { rows: rows }
                ,
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfReceive(token:any, refer_no: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/receive/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }


    async count_referout(token:any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/count_referout/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async count_referout_reply(token:any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/count_referout_reply/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async count_report(token:any, stdate: any, endate: any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/count_report/${stdate}/${endate}/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async count_report_reply(token:any, stdate: any, endate: any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/count_report_reply/${stdate}/${endate}/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async del_referout(token:any, refer_no: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/delete/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }
    async del_user_referout(token:any, refer_no: any, providerUser: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/delete_refer/${refer_no}/${providerUser}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfout_select_telemed(token:any, hcode: any, sdate: any, edate: any, limit: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/select_telemed/${hcode}/${sdate}/${edate}/${limit}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });

    }
    async rfUpdateExpire(token:any, refer_no: any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'PUT',
                url: `${urlApi}/referout/updateExpire/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                body: { rows: rows }
                ,
                json: true
            };

            request(options, function (error:any, response:any, body:any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }
}