import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisHiModel {

    async getLogin(db: Knex, username: any, password: any) {
        let data = await db.raw(`
        SELECT dct.dct as username , CONCAT(dct.fname, ' ', dct.lname) as fullname, (select hcode from setup limit 1) as hcode
        from dct WHERE dct.dct = '${username}' 
        and SUBSTRING(dct.cid, 10, 13) = '${password}'
        `);
        return data[0];
    }

    async getServices(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        SELECT 
        o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        c.namecln as department
        FROM ovst as o 
        LEFT JOIN cln as c ON c.cln = o.cln 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.hn ='${hn}' and o.vn = '${seq}' and o.an = '0'
		UNION
		SELECT 
		o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        idpm.nameidpm as department
		from ipt 
		LEFT JOIN idpm  on idpm.idpm = ipt.ward
		INNER JOIN ovst as o on o.vn = ipt.vn
	    INNER JOIN pt as p	ON p.hn = o.hn
        WHERE ipt.hn ='${hn}' and ipt.vn = '${seq}'
        `);
        return data[0];
    }

    async getProfile(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        select distinct p.hn as hn, p.pop_id as cid, 
        if(p.pname = '',
        cast(
            (case p.male 
                when 1 then if(p.mrtlst < 6,
                    if(timestampdiff(YEAR,p.brthdate,now()) < 15,'ด.ช.','นาย'),
                    if((timestampdiff(YEAR,p.brthdate,now()) < 20),'เณร','พระ')) 
                when 2 then if((p.mrtlst = 1),
                    if((timestampdiff(YEAR,p.brthdate,now()) < 15),'ด.ญ.','น.ส.'),
                    if((p.mrtlst < 6),'นาง','แม่ชี')) 
            end) as char(8) charset utf8),
        convert(p.pname using utf8)) as title_name
        ,p.fname as first_name,p.lname as last_name
        ,p.moopart,p.addrpart,p.tmbpart,amppart,chwpart,p.brthdate
        ,concat(lpad(timestampdiff(year,p.brthdate,now()),3,'0'),'-'
        ,lpad(mod(timestampdiff(month,p.brthdate,now()),12),2,'0'),'-'
        ,lpad(if(DAYOFMONTH(p.brthdate)>DAYOFMONTH(now())
        ,day(LAST_DAY(SUBDATE(now(),INTERVAL 1 month)))-DAYOFMONTH(p.brthdate)+DAYOFMONTH(now())
        ,DAYOFMONTH(now())-DAYOFMONTH(p.brthdate)),2,'0')) as age
        ,if(p.male = 1,'ชาย','หญิง') as sex
        ,m.namemale as sexname,j.nameoccptn as occupation
        ,o.pttype as pttype_id,t.namepttype as pttype_name,s.card_id as pttype_no,s.hospmain
        ,c.hosname as hospmain_name,s.hospsub,h.hosname as hospsub_name,p.fdate as registdate,p.ldate as visitdate
        ,p.fthname as father_name,p.mthname as mother_name,p.couple as couple_name,p.infmname as contact_name,p.statusinfo as contact_relation,p.infmtel as contact_mobile
        FROM pt as p 
        left join ovst as o on o.vn = '${seq}'
        INNER JOIN pttype as t on p.pttype=t.pttype
        left join male as m on p.male=m.male
        left join occptn as j on p.occptn=j.occptn
        left join insure as s on p.hn=s.hn and p.pttype=s.pttype
        left join chospital as c on s.hospmain=c.hoscode
        left join chospital as h on s.hospmain=h.hoscode
        WHERE p.hn ='${hn}'
        `);
        return data[0];
    }

    async getHospital(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT s.hcode as provider_code,h.namehosp as provider_name 
        FROM setup as s 
        INNER JOIN hospcode as h on h.off_id = s.hcode
        `);
        return data[0];
    }

    async getAllergyDetail(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT namedrug as drug_name, replace(replace(detail,char(13),' '),char(10),' ') as symptom ,entrydate as begin_date,entrydate as daterecord
        FROM allergy 
        WHERE hn ='${hn}'
        `);
        return data[0];
    }

    async getChronic(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT c.chronic as icd_code, c.date_diag as start_date, IF(i.name_t!='', i.name_t, "-") as icd_name
        FROM chronic as c 
        INNER JOIN icd101 as i on i.icd10 = c.chronic
        WHERE c.pid ='${hn}'
        `);
        return data[0];
    }

    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv
        , o.icd10 as icd_code,i.icd10name as icd_name,'1' as diag_type, d.diagtext as DiagNote,'1' as diagtype_id
        FROM orfro as o
        INNER JOIN ovst on ovst.vn = o.vn
        INNER JOIN icd101 as i on i.icd10 = o.icd10
        left JOIN visitdiagtext as d on o.vn = d.vn 
        WHERE o.vn ='${seq}'
        Union 
        SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv
        , o.icd10 as icd_code,IF(o.icd10name!='', o.icd10name, i.icd10name) as icd_name
        , o.cnt as diag_type, d.diagtext as DiagNote, 
        '1' as diagtype_id
        FROM ovstdx as o
        INNER JOIN ovst on ovst.vn = o.vn
        INNER JOIN orfro on orfro.vn != o.vn
        INNER JOIN icd101 as i on i.icd10 = o.icd10
        left JOIN visitdiagtext as d on o.vn = d.vn 
        WHERE o.vn ='${seq}' and cnt = 1
        Union 
        SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv
        , o.icd10 as icd_code,IF(o.icd10name!='', o.icd10name, i.icd10name) as icd_name
        , o.cnt as diag_type, d.diagtext as DiagNote, 
        (case 
            when o.icd10 not between 'V0000' and 'Y9999' then 4
            when o.icd10 between 'V0000' and 'Y9999' then 5 else 4 end)  as diagtype_id
        FROM ovstdx as o
        INNER JOIN ovst on ovst.vn = o.vn
        INNER JOIN icd101 as i on i.icd10 = o.icd10
        left JOIN visitdiagtext as d on o.vn = d.vn 
        WHERE o.vn ='${seq}' and ovst.an='0' and cnt <> 1
        UNION 
        SELECT 
        dt.vn as seq,
        date_format(dt.vstdttm,'%Y-%m-%d') as date_serv,
        time(dt.vstdttm) as time_serv,
        replace(x.icdda,'.','') as icd_code,
        c.icd10name as icd_name,
        '4' as diag_type,
        d.diagtext as DiagNote,
        '4' as diag_type_id
        from dt
        inner join dtdx as x 
        on dt.dn=x.dn
        left join icd101 as c on replace(x.icdda,'.','') = c.icd10
        left join visitdiagtext as d on dt.vn=d.vn
        where dt.vn='${seq}'
        UNION 
		SELECT ipt.vn as seq
		, date_format(ipt.rgtdate,'%Y-%m-%d') as date_serv
		, time(ipt.rgttime) as time_serv
        , o.icd10 as icd_code,i.icd10name as icd_name
        , o.itemno as diag_type
		, '' as DiagNote
		, (case 
            when o.icd10 not between 'V0000' and 'Y9999' then 4
            when o.icd10 between 'V0000' and 'Y9999' then 5 else 4 end)  as diagtype_id
        FROM iptdx as o
        INNER JOIN ipt on ipt.an = o.an and o.itemno <> 1
        INNER JOIN icd101 as i on i.icd10 = o.icd10 
        WHERE ipt.vn ='${seq}'
        `);
        return data[0];
    }

    async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
    select o.vn as seq,o.an as an,ovst.hn as pid, ovst.hn as hn, 
    cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        o.dchdate as referdate, 
        o.hcode as to_hcode, 
        if(t.trauma='T',1,2) as pttype_id,
        if(t.trauma='T','Trauma','Non Trauma') as pttype_name,
        if(t.triage='' or t.triage is null,'5',t.triage) as strength_id,
        (case t.triage 
            when 1 then 'Resucitate'
            when 2 then 'Emergency'
            when 3 then 'Urgency'
            when 4 then 'Semi Urgency'
            when 5 then 'Non Urgency'
            else 'Non Urgency' end 
            ) as strength_name,
  if(ovst.cln='' or ovst.cln is null,'',c.namecln) as location_name,
  if(ovst.cln='' or ovst.cln is null,'IPD',if(substr(ovst.cln,1,1)=2,'ER','OPD')) as station_name,
  '' as loads_id, 
       '' as loads_name,
        h.namehosp as to_hcode_name, 
        '' as refer_cause,
     time(o.dchtime*100) as refertime,
     if(ovst.dct='' or ovst.dct is null,dentist.lcno,d.lcno)  as doctor,
     if(ovst.dct='' or ovst.dct is null,CONCAT(dentist.fname,' ',dentist.lname),CONCAT(d.fname,' ',d.lname))  as doctor_name
     ,replace(replace(concat(substr(referresul,instr(referresul,'CC'),instr(referresul,'PE')-instr(referresul,'CC')),
     substr(referresul,instr(referresul,'PrX')),ifnull(va.advice,''),ifnull(vc.detail,'')),char(13),'\n'),char(10),' ') as refer_remark
     from referresult as o
        left Join hospcode as h on h.off_id = o.hcode
        inner join ovst on ovst.vn = o.vn
        left outer join cln as c on ovst.cln=c.cln 
        left outer Join dct as d on (case length(ovst.dct) when 5 then d.lcno=ovst.dct when 4 then substr(ovst.dct,1,2) = d.dct end) 
        left outer Join dt on dt.vn=ovst.vn
        left outer Join dtdx on dtdx.dn=dt.dn
        left outer Join dentist on dentist.codedtt=dt.dnt    
        left outer join optriage as t on o.vn=t.vn
        left outer join visitadvice as va on o.vn=va.vn 
        left outer join visitnoteconsult as vc on o.vn=vc.vn        
        where o.vn  ='${seq}' and cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}'
    UNION
    select o.vn as seq,o.an as an,ovst.hn as pid, ovst.hn as hn, 
    cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        o.vstdate as referdate, 
        o.rfrlct as to_hcode, 
        if(t.trauma='T',1,2) as pttype_id,
        if(t.trauma='T','Trauma','Non Trauma') as pttype_name,
        if(t.triage='' or t.triage is null,'5',t.triage) as strength_id,
        (case t.triage 
            when 1 then 'Resucitate'
            when 2 then 'Emergency'
            when 3 then 'Urgency'
            when 4 then 'Semi Urgency'
            when 5 then 'Non Urgency'
            else 'Non Urgency' end 
            ) as strength_name,
        if(o.cln='' or o.cln is null,i.nameidpm,c.namecln) as location_name,
        if(o.cln='' or o.cln is null,'IPD',if(substr(o.cln,1,1)=2,'ER','OPD')) as station_name,
        if(o.loads='' or o.loads is null,'4',o.loads) as loads_id, 
        if(lo.nameloads='' or lo.nameloads is null,'ไปเอง',lo.nameloads) as loads_name,
        h.namehosp as to_hcode_name, 
        concat(ifnull(f.namerfrcs,''),' Consult : ',ifnull(sp.nametype,'')) as refer_cause,
        time(o.vsttime*100) as refertime,
	     if(ovst.cln!='40100',if(ovst.an='0',(if(ovst.dct='' or ovst.dct is null,dentist.lcno,d.lcno)),iptdx.dct),dentist.lcno)  as doctor,
        if(ovst.cln!='40100',if(ovst.dct='' or ovst.dct is null,CONCAT(dentist.fname,' ',dentist.lname),(if(ovst.an='0',CONCAT(d.fname,' ',d.lname),(CONCAT(dct.fname,' ',dct.lname))))),CONCAT(dentist.fname,' ',dentist.lname)) as doctor_name
        ,replace(replace(vc.detail,char(13),'\n'),char(10),' ') as refer_remark
        from orfro as o
        left Join hospcode as h on h.off_id = o.rfrlct
        inner join ovst on ovst.vn = o.vn
        left Join rfrcs as f on f.rfrcs = o.rfrcs
				left outer join cln as c on o.cln=c.cln 
				left outer join idpm as i on o.ward=i.idpm
        left outer Join dct as d on (case length(ovst.dct) when 5 then d.lcno=ovst.dct when 4 then substr(ovst.dct,1,2) = d.dct end)
        left outer Join dt on dt.vn=ovst.vn
        left outer Join dtdx on dtdx.dn=dt.dn        
				left outer Join ipt on ipt.vn=o.vn
				left outer Join iptdx on iptdx.an=ipt.an
				left outer Join dct on dct.lcno=iptdx.dct
        left outer Join dentist on dentist.codedtt=dt.dnt    
        left outer join optriage as t on o.vn=t.vn
        left outer join l_loads as lo on o.loads = lo.codeloads
        left outer join l_rfrtype as sp on o.rfrtype=sp.rfrtype 
        left join visitnoteconsult as vc on o.vn=vc.vn
        where o.vn  ='${seq}' and cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}' limit 1
        `);
        return data[0];
    }

    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        select p.vn as seq,
        DATE_FORMAT(date(p.prscdate),'%Y%m%d') as date_serv,
        DATE_FORMAT(time(p.prsctime),'%h:%i:%s') as time_serv, 
        pd.nameprscdt as drug_name,
        pd.qty as qty, 
        med.pres_unt as unit ,
        if(pd.medusage = '',x.doseprn,IF(m.doseprn1!='', m.doseprn1, '')) as usage_line1 ,
        IF(m.doseprn2!='', m.doseprn2, '') as usage_line2,
        '' as usage_line3
        FROM prsc as p 
        left Join prscdt as pd ON pd.PRSCNO = p.PRSCNO 
        left Join medusage as m ON m.dosecode = pd.medusage
        left join xdose as x ON pd.xdoseno=x.xdoseno
        left Join meditem as med ON med.meditem = pd.meditem  
        WHERE p.vn = '${seq}' and med.type in (1,3,5) order by date_serv DESC
        `);
        return data[0];
    }

    async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT date_serv,time_serv,labgroup,lab_name,lab_result,unit,standard_result 
		FROM (
		(SELECT r.date_serv,r.time_serv,r.labgroup,r.lab_name,
        r.lab_result,r.unit,r.standard_result
        FROM
        (SELECT
        date_serv,time_serv,labname as labgroup,lab_test as lab_name,
        Get_Labresult(t.lab_table,t.labfield,t.lab_number) as lab_result,unit,
        reference as standard_result
        FROM
        (SELECT DISTINCT
        l.ln as lab_number,
        l.vn as seq,
        l.hn as hn,
        lb.labname,
        DATE_FORMAT(date(l.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(l.vstdttm),'%h:%i:%s') as time_serv,
        lb.fieldname as lab_code_local,
        ifnull(lb.unit,'') as unit,
        replace(lb.fieldlabel,"'",'\`') as lab_test, lb.filename as lab_table,
        lb.fieldname as labfield,
        concat(ifnull(lb.normal,''),' ',ifnull(lb.unit,'')) as reference,
        replace(lab.labname,"'",'\`') as lab_group_name,
        l.labcode as lab_group
        FROM
        lbbk as l 
        inner join lab on l.labcode=lab.labcode and l.finish=1 and l.vn='${seq}'
        inner join lablabel as lb on l.labcode = lb.labcode
        group by l.ln,l.labcode,lb.filename,lb.fieldname
        order by date_serv DESC
        ) as t ) as r 
		where length(RTRIM(LTRIM(r.lab_result))) > 0 
        order by labgroup,lab_name,date_serv,time_serv )	
		UNION
		(select 
		DATE_FORMAT(date(l.vstdttm),'%Y-%m-%d') as date_serv, 
		DATE_FORMAT(time(l.vstdttm),'%h:%i:%s') as time_serv,
		r.lab_name as labgroup,
		replace(lb.fieldlabel,"'",'\`') as lab_name,
		replace(replace(r.labresult,char(13),' '),char(10),' ') as lab_result,
		r.unit,
		r.normal as standard_result
		from 
		labresult as r
		inner join lbbk as l on r.ln=l.ln and l.finish=1
		inner join lablabel as lb on r.labcode = lb.labcode and r.lab_code_local=lb.fieldname
		where l.vn = '${seq}' and (r.labcode != NULL or r.labcode !=''))) x
		GROUP BY date_serv,time_serv,lab_name
		ORDER BY date_serv,time_serv,labgroup 
        `
        );
        return data[0];
    }

    async getAppointment(db: Knex, hn: any, dateServ: any, seq: any) {
        let data = await db.raw(`
        SELECT o.vn as seq, o.vstdate as date_serv, o.fudate as date, o.futime as time, o.cln as department, o.dscrptn as detail, time(ovst.vstdttm) as time_serv
        FROM oapp as o 
        INNER JOIN ovst on ovst.vn = o.vn
        WHERE o.vn ='${seq}'
        `);
        return data[0];
    }

    async getVaccine(db: Knex, hn: any) {
        let data = await db.raw(`
        select 
        o.vstdttm as date_serv,
        DATE_FORMAT(time(o.drxtime),'%h:%i:%s') as time_serv, 
        h.vac as vaccine_code, 
        h.namehpt as vaccine_name
        from 
        epi e 
        inner join 
        ovst o on e.vn = o.vn 
        left join 
        hpt as h on e.vac=h.codehpt
        where 
        o.hn='${hn}'
        
        UNION

        select 
        o.vstdttm as date_serv,
        DATE_FORMAT(time(o.drxtime),'%h:%i:%s') as time_serv, 
        vc.stdcode as vacine_code, 
        vc.\`name\` as vacine_name
        from 
        ovst o 
        inner join 
        prsc pc on o.vn = pc.vn  
        inner join 
        prscdt pd on pc.prscno = pd.prscno  
        inner join 
        meditem m on pd.meditem = m.meditem 
        inner join 
        vaccine vc on vc.meditem = m.meditem  
        where 
        o.hn='${hn}'
        `);
        return data[0];
    }
    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT
        o.hn as pid,
        o.vn as seq,
        DATE_FORMAT(date(o.vstdttm),'%Y%m%d') as date_serv,	
        DATE_FORMAT(time(o.nrxtime),'%h:%i:%s') as time_serv, 
        p.icd9cm as procedure_code,	
        p.icd9name as procedure_name,
        DATE_FORMAT(date(p.opdttm),'%Y%m%d') as start_date,	
        DATE_FORMAT(time(p.opdttm),'%h:%i:%s') as start_time,
        DATE_FORMAT(date(p.opdttm),'%Y%m%d') as end_date,
        '00:00:00' as end_time
        from
        ovst o 
        inner join 
        ovstdx ox on o.vn = ox.vn 
        inner join
        oprt p on o.vn = p.vn 
        left outer join
        cln c on o.cln = c.cln
        LEFT OUTER JOIN 
        dct on (
            CASE WHEN LENGTH(o.dct) = 5 THEN dct.lcno = o.dct 
                WHEN LENGTH(o.dct) = 4 THEN dct.dct = substr(o.dct,1,2)  
                WHEN LENGTH(o.dct) = 2 THEN dct.dct = o.dct END )
        where 
        o.vn = '${seq}' and p.an = 0 
        group by 
        p.vn,procedure_code
        UNION 
        SELECT 
        o.hn as pid,
        o.vn as seq,
        DATE_FORMAT(date(o.vstdttm),'%Y%m%d') as date_serv,
        DATE_FORMAT(time(o.nrxtime),'%h:%i:%s') as time_serv, 
        i.ICD10TM as procedure_code,
        i.name_Tx as procedure_name,
        DATE_FORMAT(date(dt.vstdttm),'%Y%m%d') as start_date,	
        DATE_FORMAT(time(dt.vstdttm),'%h:%i:%s') as start_time,
        DATE_FORMAT(date(dt.vstdttm),'%Y%m%d') as end_date,
        '00:00:00' as end_time
    
        FROM
        dtdx 
        INNER JOIN 
        icd9dent as i on dtdx.dttx=i.code_tx
        INNER JOIN 
        dt on dtdx.dn=dt.dn
        INNER JOIN
        ovst as o on dt.vn=o.vn and o.cln='40100'
        left outer join 
        cln c on o.cln = c.cln  
        left join dentist as d on dt.dnt=d.codedtt
        where 
        o.vn = '${seq}'
        group by 
        dtdx.dn,procedure_code
        `);
        return data[0];
    }

    async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT x.seq,x.date_serv,x.time_serv,x.bloodgrp,x.weight,x.height,x.bmi,x.temperature,x.pr,x.rr,x.sbp,x.dbp,
        x.symptom,x.depcode,x.department,x.movement_score,x.vocal_score,x.eye_score,x.oxygen_sat,x.gak_coma_sco,x.diag_text,x.pupil_right,x.pupil_left
        FROM (
		select o.vn as seq,
        DATE_FORMAT(date(o.rgtdate),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.rgttime),'%h:%i:%s') as time_serv, 
        p.bloodgrp as bloodgrp,
        o.bw as weight,
        o.height as height,
        o.bmi as bmi,
        ifnull(t.tt,'') as temperature,
        ifnull(t.pr,'') as pr,
        ifnull(t.rr,'') as rr,
        ifnull(t.sbp,'') as sbp,
        ifnull(t.dbp,'') as dbp,
		concat(ifnull(group_concat(cc.symptom  order by cc.id separator ' '),''),ifnull(group_concat(cc_d.symptom  order by cc_d.id separator ' '),'')) as symptom,
        c.idpm as depcode,
        c.nameidpm as department,
        '' as movement_score,'' as vocal_score,'' as eye_score,'' as oxygen_sat,'' as gak_coma_sco,
		vd.diagtext as diag_text,'' as pupil_right, '' as pupil_left
        FROM ipt as o 
		LEFT JOIN visitdiagtext as vd on o.vn = vd.vn 
		LEFT JOIN symptm as cc on o.vn = cc.vn
        LEFT JOIN dt on o.vn=dt.vn and o.an=0
        LEFT JOIN symp_d as cc_d on dt.dn=cc_d.dn 
        left join (select * from tpr order by dttm desc) as t on o.an=t.an
        LEFT JOIN idpm as c ON c.idpm = o.ward 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.vn = '${seq}'
		UNION
		select o.vn as seq,
        DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        p.bloodgrp as bloodgrp,
        o.bw as weight,
        o.height as height,
        o.bmi as bmi,
        o.tt as temperature,
        o.pr as pr,
        o.rr as rr,
        o.sbp as sbp,
        o.dbp as dbp,
        concat(ifnull(group_concat(cc.symptom  order by cc.id separator ' '),''),ifnull(group_concat(cc_d.symptom  order by cc_d.id separator ' '),'')) as symptom,
        c.cln as depcode,
        c.namecln as department,
        s.m as movement_score,s.v as vocal_score,s.e as eye_score,s.o2sat as oxygen_sat,s.sos as gak_coma_sco,
		vd.diagtext  as diag_text,'' as pupil_right, '' as pupil_left
        FROM ovst as o 
        LEFT JOIN visitdiagtext as vd on o.vn = vd.vn        
		LEFT JOIN symptm as cc on o.vn = cc.vn
        LEFT JOIN dt on o.vn=dt.vn and o.an=0
        LEFT JOIN symp_d as cc_d on dt.dn=cc_d.dn 
        LEFT JOIN visitsosscore as s on o.vn=s.vn
        LEFT JOIN cln as c ON c.cln = o.cln 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.vn = '${seq}' and o.an = 0 
		) x WHERE x.seq is not NULL  and x.seq !='0'
        `);
        return data[0];
    }


    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT r.seq,group_concat(r.pe) as pe FROM (
        SELECT d.vn as seq , group_concat(s.sign order by id separator '\n') as pe FROM dt as d inner join sign_d as s on d.dn=s.dn WHERE d.vn = '${seq}'
        UNION 
        SELECT s.vn as seq , group_concat(s.sign order by id separator '\n') as pe FROM sign as s INNER join ovst as o on o.vn=s.vn WHERE s.vn = '${seq}' 
        ) as r WHERE seq is not null  and seq !='0' group by r.seq
        `);
        return data[0];
    }

    async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT p.vn as seq , group_concat(p.pillness order by id separator '\n') as hpi FROM pillness as p inner join ovst as o on p.vn=o.vn and o.an=0 WHERE p.vn = '${seq}'  group by p.vn
        `);
        return data[0];
    }

    async getBedsharing(db: Knex) {
        let data = await db.raw(`
        select 
        idpm.idpm as ward_code,
        idpm.nameidpm as ward_name,
        count(an) as ward_pt,
        idpm.bed as ward_bed,
        idpm.bed_std as ward_standard 
        from 
        idpm 
        left join ipt on idpm.idpm=ipt.ward
        and dchdate = '0000-00-00'
        group by idpm
        `);
        return data[0];
    }

    async getReferOut(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select o.vn as seq, pt.hn as hn, o.an as an,
        pt.pname as title_name, pt.fname as first_name, pt.lname as last_name,
        cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        date(o.vstdate) as referdate,
        if(o.cln='' or o.cln is null,i.nameidpm,c.namecln) as location_name,
        o.rfrlct as to_hcode,
        if(t.trauma='T',1,2) as pttype_id,
        if(t.trauma='T','Trauma','Non Trauma') as pttype_name,
        t.triage as strength_id,
        (case t.triage 
            when 1 then 'Resucitate'
            when 2 then 'Emergency'
            when 3 then 'Urgency'
            when 4 then 'Semi Urgency'
            when 5 then 'Non Urgency'
            else '' end 
            ) as strength_name,
        h.namehosp as to_hcode_name,
        concat(ifnull(f.namerfrcs,''),' Consult:',rt.nametype) as refer_cause,
        time(o.vsttime*100) as refertime,
        d.lcno as doctor,
        CONCAT(d.fname, ' ', d.lname) as doctor_name
        ,replace(replace(vc.detail,char(13),' '),char(10),' ') as refer_remark
        from orfro as o
        left Join hospcode as h on h.off_id = o.rfrlct
        inner join ovst on ovst.vn = o.vn
        inner Join pt on pt.hn = ovst.hn
        left join cln as c on o.cln=c.cln 
        left join idpm as i on o.ward=i.idpm
        left Join rfrcs as f on f.rfrcs = o.rfrcs
        left Join dct as d on (case length(ovst.dct) when 5 then d.lcno=ovst.dct when 4 then substr(ovst.dct,1,2) = d.dct end)
        left join optriage as t on o.vn=t.vn
        left join l_rfrtype as rt on o.rfrtype=rt.rfrtype
        left join visitnoteconsult as vc on o.vn=vc.vn
        where date(o.vstdate) between '${start_date}' and '${end_date}'  GROUP BY seq,referno
        `);
        return data[0];
    }

    async getReferBack(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select o.vn as seq, pt.hn as hn, o.an as an,
        pt.pname as title_name,
        pt.fname as first_name, 
        pt.lname as last_name,
        cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        date(ovst.vstdttm) as referdate,
        o.hcode as to_hcode,
        h.namehosp as to_hcode_name,
        '' as refer_cause,
        time(ovst.vstdttm*100) as refertime,
        d.lcno as doctor,
        CONCAT(d.fname, ' ', d.lname) as doctor_name
        from referresult as o
        inner Join hospcode as h on h.off_id = o.hcode
        INNER join ovst on ovst.vn = o.vn
        INNER join ovstdx on ovst.vn=ovstdx.vn and ovstdx.cnt = 1 
        INNER Join pt on pt.hn = ovst.hn
        LEFT Join dct as d on d.lcno = ovst.dct
        where date(ovst.vstdttm) between '${start_date}' and '${end_date}' 
        union
        select o.vn as seq, pt.hn as hn, o.an as an,
        pt.pname as title_name,
        pt.fname as first_name, 
        pt.lname as last_name,
        cast(replace(o.rfrno,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
        date(o.dchdate) as referdate,
        o.hcode as to_hcode,
        h.namehosp as to_hcode_name,
        '' as refer_cause,
        time(o.dchtime*100) as refertime,
        d.lcno as doctor,
        CONCAT(d.fname, ' ', d.lname) as doctor_name
        from referresult as o
        inner Join hospcode as h on h.off_id = o.hcode
        inner join ipt on ipt.an = o.an
        inner join iptdx on ipt.an=iptdx.an and itemno=1 
        inner Join pt on pt.hn = ipt.hn
        LEFT Join dct as d on d.lcno = iptdx.dct
        where o.dchdate between '${start_date}' and '${end_date}'
        `);
        return data[0];
    }

    async getAppoint(db: Knex, hn: any, app_date: any) {
        let data = await db.raw(`
        SELECT 
        a.vn as seq,
        a.fudate as receive_apppoint_date,
        '' as receive_apppoint_beginttime,
        '' as receive_apppoint_endtime,
        CONCAT(d.dspname,' ',l.prename,d.fname,' ',d.lname) as receive_apppoint_doctor,
        group_concat(distinct c.namecln) as receive_apppoint_chinic, 
        group_concat(distinct a.dscrptn) as receive_text,
        '' as receive_by
        from oapp as a 
        inner join pt as p on a.hn=p.hn
        left join dct as d on (case length(a.dct) when 5 then a.dct=d.lcno when 4 then substr(a.dct,3,2)=d.dct end)
        left join l_prename as l on d.pname=l.prename_code
        left join lab on substr(a.cln,2,3)=lab.labcode
        left join cln as c on if(substr(a.cln,1,1) !=7,a.cln=c.cln,c.cln='70100')
        WHERE a.hn ='${hn}' and a.vstdate = '${app_date}'
        `);
        return data[0];
    }

    async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT xryrgt.vstdate as xray_date ,
        xray.xryname as xray_name 
        FROM xryrgt 
        INNER JOIN xray on xray.xrycode = xryrgt.xrycode
        WHERE xryrgt.vn = '${seq}'
        `);
        return data[0];
    }

    async getDepartment(db: Knex) {
        let data = await db.raw(`
        select cln as dep_code,namecln as dep_name from cln
        `);
        return data[0];
    }

    async getPtHN(db: Knex, cid: any) {
        let data = await db.raw(`
        select hn from pt where pt.pop_id = '${cid}'
        `);
        return data[0][0];
    }

    async getMedrecconcile(db: Knex, hn: any) {
        let data = await db.raw(`
        select '' as drug_hospcode ,hospname as drug_hospname,medname as drug_name,meduse as drug_use,lastdate as drug_receive_date from medreconcile where hn = '${hn}'
        `);
        return data[0];
    }

    async getServicesCoc(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT o.hn,
        o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        c.namecln as department
        FROM ovst as o 
        LEFT JOIN cln as c ON c.cln = o.cln 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.hn ='${hn}' and o.an = '0'
		UNION
		SELECT o.hn,
		o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        idpm.nameidpm as department
		from ipt 
		LEFT JOIN idpm  on idpm.idpm = ipt.ward
		INNER JOIN ovst as o on o.vn = ipt.vn
	    INNER JOIN pt as p	ON p.hn = o.hn
        WHERE ipt.hn ='${hn}'
        ORDER BY seq DESC limit 3
        `);
        return data[0];
    }

    async getProfileCoc(db: Knex, hn: any) {
        let data = await db.raw(`
        select p.hn as hn, p.pop_id as cid, 
        if(p.pname = '',
        cast(
            (case p.male 
                when 1 then if(p.mrtlst < 6,
                    if(timestampdiff(YEAR,p.brthdate,now()) < 15,'ด.ช.','นาย'),
                    if((timestampdiff(YEAR,p.brthdate,now()) < 20),'เณร','พระ')) 
                when 2 then if((p.mrtlst = 1),
                    if((timestampdiff(YEAR,p.brthdate,now()) < 15),'ด.ญ.','น.ส.'),
                    if((p.mrtlst < 6),'นาง','แม่ชี')) 
            end) as char(8) charset utf8),
        convert(p.pname using utf8)) as title_name
        ,p.fname as first_name,p.lname as last_name
        ,p.moopart,p.addrpart,p.tmbpart,amppart,chwpart,p.brthdate
        ,concat(lpad(timestampdiff(year,p.brthdate,now()),3,'0'),'-'
        ,lpad(mod(timestampdiff(month,p.brthdate,now()),12),2,'0'),'-'
        ,lpad(if(DAYOFMONTH(p.brthdate)>DAYOFMONTH(now())
        ,day(LAST_DAY(SUBDATE(now(),INTERVAL 1 month)))-DAYOFMONTH(p.brthdate)+DAYOFMONTH(now())
        ,DAYOFMONTH(now())-DAYOFMONTH(p.brthdate)),2,'0')) as age
        ,if(p.male = 1,'ชาย','หญิง') as sex
        ,m.namemale as sexname,j.nameoccptn as occupation
        ,p.pttype as pttype_id,t.namepttype as pttype_name,s.card_id as pttype_no,s.hospmain
        ,c.hosname as hospmain_name,s.hospsub,h.hosname as hospsub_name,p.fdate as registdate,p.ldate as visitdate
        ,p.fthname as father_name,p.mthname as mother_name,p.couple as couple_name,p.infmname as contact_name,p.statusinfo as contact_relation,p.infmtel as contact_mobile
        FROM pt as p 
        INNER JOIN pttype as t on p.pttype=t.pttype
        left join male as m on p.male=m.male
        left join occptn as j on p.occptn=j.occptn
        left join insure as s on p.hn=s.hn and p.pttype=s.pttype
        left join chospital as c on s.hospmain=c.hoscode
        left join chospital as h on s.hospmain=h.hoscode
        WHERE p.hn ='${hn}'
        `);
        return data[0];
    }

}

