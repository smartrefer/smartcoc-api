import Knex = require('knex');
// import request = require("request");
const hospcode = process.env.HIS_CODE;

export class HisEphisHiModel {

    async propLower(objData: any) {
        let ndata:object[]=[];
        objData.forEach(function (dtRow: any) {
            let objTmp: any = {};
            (Object.keys(dtRow) as (keyof typeof dtRow)[]).forEach((key: any, index) => {
                objTmp[key.toLowerCase()] = dtRow[key];
            });
            ndata.push(objTmp);
        });
        return ndata;
    }

    // 01 login service models from his
    async getLogin(db: Knex, cid: any) {
  
        let data = await db.raw(`
        SELECT U.USERID AS username, U.DSPNAME AS fullname, '11472' AS hcode
        FROM RJVT.PHISUSER U 
        WHERE U.CARDNO = '${cid}' `);
        return this.propLower(data);
        // return data;
    }
    // 02 get refer out service models from his
    async getServices(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        SELECT OVST.VN AS seq,PNAME.FULLNAME AS title_name, PT.FNAME AS first_name, 
        PT.LNAME AS last_name, TO_CHAR(OVST.VSTDATE,'YYYY-MM-DD') AS date_serv, 
        TO_CHAR(OVST.VSTTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS time_serv, 
        CLINICLCT.DSPNAME AS department
        FROM RJVT.OVST 
        INNER JOIN RJVT.PT ON PT.HN = OVST.HN
        INNER JOIN RJVT.PNAME ON PNAME.PNAME = PT.PNAME
        INNER JOIN RJVT.CLINICLCT ON CLINICLCT.CLINICLCT = OVST.CLINICLCT
        WHERE OVST.HN = '${hn}' AND OVST.VN = '${seq}'`);
        return this.propLower(data);
        // return data;
    }

    // 03 get patient profile models from his
    async getProfile(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        SELECT PT.HN AS HN,PTNO.CARDNO AS CID, PNAME.FULLNAME AS TITLE_NAME, PT.FNAME AS FIRST_NAME, PT.LNAME AS LAST_NAME, 
        PTADDR.MOO AS MOOPART, PTADDR.ADDR || ' ' || PTADDR.SOI || ' ' || PTADDR.STREET AS ADDRPART, TUMBON.NAME AS TMBPART, 
        AMPUR.NAME AS AMPPART, CHANGWAT.NAME AS CHWPART, TO_CHAR(PT.BRTHDATE,'YYYY-MM-DD') AS BRTHDATE,
        DTOAGE(PT.BRTHDATE ,SYSDATE ,5) AS AGE ,PT_TYPE.PTTYPE AS pttype_id, PT_TYPE.NAME AS pttype_name,PT_TYPE.PRVNO AS pttype_no,
        PT_TYPE.MAINHPT AS hospmain, PT_TYPE.MAINHPT_NAME AS hospmain_name, PT_TYPE.SUPPHPT AS hospsub, PT_TYPE.SUPPHPT_NAME AS hospsub_name, 
        TO_CHAR(PT_TYPE.MX_DATE,'YYYY-MM-DD') AS registdate, TO_CHAR(PT_TYPE.MX_DATE,'YYYY-MM-DD') AS visitdate,
        PTEXT.FTHFNAME ||' ' || PTEXT.FTHLNAME AS father_name, PTEXT.MTHFNAME ||' ' || PTEXT.MTHLNAME AS mother_name, PTEXT.SPSFNAME ||' ' || PTEXT.SPSLNAME AS couple_name, 
        PTINFORMER.INFFNAME || ' ' || PTINFORMER.INFLNAME AS contact_name, PRSNRLT.NAME AS contact_relation, PTINFORMER.MOBILEPHONE AS contact_mobile
        FROM RJVT.PT 
        INNER JOIN RJVT.PTNO 
            ON PTNO.HN = PT.HN AND PTNO.NOTYPE IN ('10','20','30') 
        INNER JOIN RJVT.PNAME 
            ON PNAME.PNAME = PT.PNAME
        LEFT OUTER JOIN RJVT.PTADDR 
            ON PT.HN = PTADDR.HN AND PTADDR.ADDRTYPE = 10 AND PTADDR.ADDRFLAG = 1
        LEFT OUTER JOIN RJVT.TUMBON 
            ON PTADDR.TUMBON = TUMBON.TUMBON AND TUMBON.CANCELDATE IS NULL 
        LEFT OUTER JOIN RJVT.AMPUR 
            ON PTADDR.AMPUR = AMPUR.AMPUR AND AMPUR.CANCELDATE IS NULL 
        LEFT OUTER JOIN RJVT.CHANGWAT 
            ON PTADDR.CHANGWAT = CHANGWAT.CHANGWAT AND CHANGWAT.CANCELDATE IS NULL 
        LEFT OUTER JOIN (SELECT INCPRVLG.HN ,INCPTTYPE.MX_DATE ,INCPRVLG.PRVREF ,INCPRVLG.PTTYPE ,PTTYPE.NAME ,INCPRVLG.PRVNO, INCPRVLG.MAINHPT, HPT1.NAME AS MAINHPT_NAME, INCPRVLG.SUPPHPT, HPT2.NAME AS SUPPHPT_NAME
                        FROM RJVT.INCPRVLG
                        INNER  JOIN (SELECT HN ,MAX(ISSDATE) MX_DATE
                                        FROM RJVT.INCPRVLG 
                                        WHERE HN = '${hn}'
                                        AND CANCELDATE IS NULL
                                        AND SUBTYPE = 10
                                        AND PTTYPEST= 10
                                        GROUP BY HN
                                    ) INCPTTYPE 
                            ON INCPRVLG.HN = INCPTTYPE.HN AND INCPRVLG.ISSDATE = INCPTTYPE.MX_DATE
                        LEFT OUTER JOIN RJVT.PTTYPE
                            ON INCPRVLG.PTTYPE = PTTYPE.PTTYPE AND PTTYPE.CANCELDATE IS NULL
                        LEFT JOIN RJVT.HPT HPT1
                                ON HPT1.HPT = INCPRVLG.MAINHPT
                        LEFT JOIN RJVT.HPT HPT2
                                ON HPT2.HPT = INCPRVLG.SUPPHPT
                        WHERE INCPRVLG.HN = '${hn}'
                        AND INCPRVLG.CANCELDATE IS NULL
                        AND INCPRVLG.SUBTYPE = 10
                        AND INCPRVLG.PTTYPEST= 10) PT_TYPE
                                ON PT.HN = PT_TYPE.HN
        LEFT JOIN RJVT.PTEXT
            ON PTEXT.HN = PT.HN
        LEFT JOIN RJVT.PTINFORMER
            ON PTINFORMER.HN = PT.HN
        LEFT JOIN RJVT.PRSNRLT
            ON PRSNRLT.PRSNRLT = PTINFORMER.PRSNRLT
        WHERE PT.CANCELDATE IS NULL AND PT.HN = '${hn}'`);
        return this.propLower(data);
        // return data;
    }

    // 04 get provider models from his
    async getHospital(db: Knex, hn: any) {
        let data = [{ 
            provider_code : '11472',
            provider_name : 'โรงพยาบาลราชวิถี'
        }];
        return this.propLower(data);
        // return data;
    }

    // 05 get patient allergy models from his
    async getAllergyDetail(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT CASE
                WHEN PTALLERGY.GENERIC IS NOT NULL THEN MEDGENERIC.NAME
                WHEN PTALLERGY.GENERICOUT IS NOT NULL THEN PTALLERGY.GENERICOUT
                WHEN PTALLERGY.GENERIC IS NULL AND PTALLERGY.GENERICOUT IS NULL THEN PTALLERGY.MEDOTHER
            END AS drug_name,
            PTALLERGY.ALGYSIGN AS symptom, TO_CHAR(PTALLERGY.ALGYDATE, 'YYYY-MM-DD') AS begin_date, TO_CHAR(PTALLERGY.ALGYDATE, 'YYYY-MM-DD') AS daterecord
        FROM RJVT.PTALLERGY
        LEFT JOIN RJVT.MEDGENERIC
            ON MEDGENERIC.GENERIC = PTALLERGY.GENERIC
        WHERE PTALLERGY.CANCELDATE IS NULL AND HN = '${hn}'`);
        return this.propLower(data);
        // return data;
    }

    //06 get chronic models from his
    async getChronic(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT PTDIAG.ICD10 AS icd_code, TO_CHAR(PTDIAG.VSTDATE, 'YYYY-MM-DD') AS start_date, ICD10.NAME AS icd_name
        FROM RJVT.PTDIAG
        LEFT JOIN RJVT.ICD10
            ON ICD10.ICD10 = PTDIAG.ICD10
        WHERE 
        PTDIAG.HN = '${hn}' AND
        ICD10.CANCELDATE IS NULL AND
        ICD10.CHONICSS IS NOT NULL
        ORDER BY PTDIAG.VSTDATE DESC`);
        return this.propLower(data);
        // return data;
    }

    //07 get diagnosis models from his
    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT PTDIAG.VN AS seq, TO_CHAR(PTDIAG.VSTDATE, 'YYYY-MM-DD') AS date_serv, TO_CHAR(PTDIAG.VSTTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS time_serv,
        PTDIAG.ICD10 AS icd_code, ICD10.NAME AS cd_name, DIAGTYPE.NAME AS diag_type, PTDIAG.DIAGKEY AS DiagNote, PTDIAG.DIAGTYPE AS diagtype_id
        FROM RJVT.PTDIAG
        LEFT JOIN RJVT.ICD10
            ON ICD10.ICD10 = PTDIAG.ICD10
        LEFT JOIN RJVT.DIAGTYPE
            ON DIAGTYPE.DIAGTYPE = PTDIAG.DIAGTYPE 
        WHERE PTDIAG.VN = '${seq}'`);
        return this.propLower(data);
        // return data;
    }

    //08 get refer models from his
    async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT RT.VN AS seq, RT.AN, PTNO.CARDNO AS pid, RT.HN, RT.RFRNO AS referno, TO_CHAR(RT.RFRDATE,'YYYY-MM-DD') AS referdate, RT.RFRHPTRECEIVE AS to_hcode, 
        PT_TYPE.PTTYPE AS pttype_id, PT_TYPE.NAME AS pttype_name, NULL AS strength_id, NULL AS strength_name, NULL AS location_name, CLINICLCT.NAME AS station_name, NULL AS loads_id, NULL AS loads_name,
        PN.FULLNAME AS title_name, PT.FNAME AS first_name, PT.LNAME AS last_name, 
        HPT.NAME AS to_hcode_name, CS.NAME AS refer_cause, TO_CHAR(RT.RFRTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS refertime, 
        DCT.LCNO AS doctor, DCT.DSPNAME AS doctor_name, RT.REMARK AS refer_remark
        FROM RJVT.RFRVST RT 
        INNER JOIN RJVT.PT 
            ON PT.HN = RT.HN
        INNER JOIN RJVT.PTNO 
            ON PT.HN = PTNO.HN AND PTNO.NOTYPE IN ('10','20','30') 
        INNER JOIN RJVT.PNAME PN
            ON PN.PNAME = PT.PNAME
        LEFT JOIN RJVT.RFRCS CS
            ON CS.RFRCS = RT.RFRCS
        LEFT JOIN RJVT.HPT
            ON HPT.HPT = RT.RFRHPTRECEIVE
        LEFT JOIN RJVT.DCT
            ON DCT.DCT = RT.DCT
        LEFT JOIN RJVT.CLINICLCT
            ON CLINICLCT.CLINICLCT = RT.RFRLCT
        LEFT OUTER JOIN (SELECT INCPRVLG.HN ,INCPTTYPE.MX_DATE ,INCPRVLG.PRVREF ,INCPRVLG.PTTYPE ,PTTYPE.NAME ,INCPRVLG.PRVNO, 
                            INCPRVLG.MAINHPT, HPT1.NAME AS MAINHPT_NAME, INCPRVLG.SUPPHPT, HPT2.NAME AS SUPPHPT_NAME
                            FROM RJVT.INCPRVLG
                            INNER  JOIN (SELECT HN ,MAX(ISSDATE) MX_DATE
                                            FROM RJVT.INCPRVLG 
                                            WHERE CANCELDATE IS NULL
                                            AND SUBTYPE = 10
                                            AND PTTYPEST= 10
                                            GROUP BY HN
                                            ) INCPTTYPE 
                                    ON INCPRVLG.HN = INCPTTYPE.HN AND INCPRVLG.ISSDATE = INCPTTYPE.MX_DATE
                            LEFT OUTER JOIN RJVT.PTTYPE
                                ON INCPRVLG.PTTYPE = PTTYPE.PTTYPE AND PTTYPE.CANCELDATE IS NULL
                            LEFT JOIN RJVT.HPT HPT1
                                ON HPT1.HPT = INCPRVLG.MAINHPT
                            LEFT JOIN RJVT.HPT HPT2
                                ON HPT2.HPT = INCPRVLG.SUPPHPT
                            WHERE INCPRVLG.CANCELDATE IS NULL
                            AND INCPRVLG.SUBTYPE = 10
                            AND INCPRVLG.PTTYPEST= 10) PT_TYPE
            ON RT.HN = PT_TYPE.HN
        WHERE RT.VN = '${seq}' AND RT.RFRNO = '${referno}'
        AND RT.CANCELDATE IS NULL
        `);
        return this.propLower(data);
    }

    //09 get medicine models from his
    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT PRSC.VN AS seq, TO_CHAR(PRSC.PRSCDATE,'YYYY-MM-DD') AS date_serv,TO_CHAR(PRSC.PRSCTIME , 'FM00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS time_serv, MEDITEM.MEDNAME AS drug_name, PRSCDT.QTY AS qty, 
        CASE 
            WHEN PRSCDT.MEDCD IS NOT NULL 
            THEN PRSCDT.MEDCD 
                WHEN PRSCDT.MEDCD IS NULL 
                THEN PRSCDT.MEDCD2 
        END AS unit, NULL AS usage_line1, NULL AS usage_line2, NULL AS usage_line3 
        FROM RJVT.PRSC , RJVT.PRSCDT , RJVT.MEDITEM , RJVT.SPHMLCT
        WHERE RJVT.PRSC.SPHMLCT = RJVT.PRSCDT.SPHMLCT(+)
        AND RJVT.PRSC.PRSCNO = RJVT.PRSCDT.PRSCNO(+)
        AND RJVT.PRSC.PRSCST = (40)
        AND RJVT.PRSCDT.MEDITEM = RJVT.MEDITEM.MEDITEM(+)
        AND RJVT.PRSC.SPHMLCT = RJVT.SPHMLCT.SPHMLCT
        AND RJVT.SPHMLCT.VARCODE = 'SPHMLCT'
        AND RJVT.SPHMLCT.CANCELDATE IS NULL
        AND RJVT.PRSC.VN = '${seq}'
        ORDER BY RJVT.PRSC.PRSCDATE , RJVT.PRSC.PRSCTIME`);
        return this.propLower(data);
        // return data;
    }

    //10 get investigation models from his
    async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT TO_CHAR(LVST.LVSTDATE,'YYYY-MM-DD') AS date_serv, TO_CHAR(LVST.LVSTTIME , 'FM00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS time_serv , 
        LABGRP.NAME AS labgroup, LABEXM.NAME AS lab_name ,LVSTEXM.RESULT ASlab_result
        ,LVSTEXM.MINNRM || '-' || LVSTEXM.MAXNRM ||' '|| LVSTEXM.NRMUNIT AS standard_result
        FROM RJVT.LVST, RJVT.LVSTEXM , RJVT.LABEXM , RJVT.LVSTST, RJVT.LABGRP
        WHERE RJVT.LVST.HN = RJVT.LVSTEXM.HN(+)
        AND RJVT.LVST.LVSTDATE = RJVT.LVSTEXM.LVSTDATE(+)
        AND RJVT.LVST.LVSTTIME = RJVT.LVSTEXM.LVSTTIME(+)
        AND RJVT.LVST.LABGRP = RJVT.LVSTEXM.LABGRP(+)
        AND RJVT.LABEXM.LABGRP = RJVT.LABGRP.LABGRP
        AND RJVT.LVST.GRPNO = RJVT.LVSTEXM.GRPNO(+)
        AND RJVT.LVSTEXM.LABEXM = RJVT.LABEXM.LABEXM(+)
        AND RJVT.LVST.LVSTST = RJVT.LVSTST.LVSTST
        AND RJVT.LVSTST.CANCELDATE IS NULL
        AND RJVT.LVST.VN = '${seq}'
        AND RJVT.LABEXM.RESULTST = 1
        ORDER BY RJVT.LVST.LVSTDATE , RJVT.LVST.LVSTTIME`);
        return this.propLower(data);
        // return data;
    }

    //11 get appointment models from his
    async getAppointment(db: Knex, hn: any, dateServ: any, seq: any) {
        let data = await db.raw(`
        SELECT V.VN AS seq, TO_CHAR(A.OAPPDATE,'YYYY-MM-DD') AS date_serv, TO_CHAR(A.OAPPTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS time_serv,
        TO_CHAR(A.NEXTDATE,'YYYY-MM-DD') AS "date", TO_CHAR(A.NEXTTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS time, 
        C.NAME AS department, A.NOTE AS detail
        FROM RJVT.OAPP A, RJVT.OVST V, RJVT.CLINICLCT C
        WHERE A.FN = V.FN
        AND A.NEXTLCT = C.CLINICLCT 
        AND V.VN = '${seq}'`);
        return this.propLower(data);
        // return data;
    }

    //12 get vaccine models from his
    async getVaccine(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT PTVACCINE.HN,
            PTVACCINE.VSTDATE AS date_serv,
            PTVACCINE.VSTTIME AS time_serv,
            PTVACCINE.VACCINE AS vaccine_code,
            VACCINE.NAME AS vaccine_name
        FROM RJVT.PTVACCINE
        LEFT OUTER JOIN RJVT.VACCINE 
            ON PTVACCINE.VACCINE = VACCINE.VACCINE
        WHERE HN = '${hn}'`);
        return this.propLower(data);
        // return data;
    }

    //13 get procedure models from his
    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT O.CARDNO, T.VN, TO_CHAR(T.OPRTDATE,'YYYY-MM-DD') AS date_serv, TO_CHAR(T.OPRTTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS time_serv, 
        I.ICD9CM AS procedure_code, I.NAME AS procedure_name,
        TO_CHAR(T.OPRTDATEIN,'YYYY-MM-DD') AS start_date, TO_CHAR(T.OPRTTIMEIN, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS start_time, 
        TO_CHAR(T.OPRTDATEOUT,'YYYY-MM-DD') AS end_date, TO_CHAR(T.OPRTTIMEOUT, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS end_time
        FROM RJVT.PTOPRT T
        INNER JOIN RJVT.PTNO O 
            ON O.HN = T.HN AND O.NOTYPE IN ('10','20','30') 
        LEFT JOIN RJVT.ICD9CM I
            ON I.ICD9CM = T.ICD9CM
        WHERE T.VN = '${seq}' 
            AND T.CANCELDATE IS NULL`);
        return this.propLower(data);
        // return data;
    }

    //14 get vital sign and service clinic models from his
    async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT 
            OVST.VN AS SEQ, 
            TO_CHAR(OVST.VSTDATE,'YYYY-MM-DD') AS DATE_SERV,
            TO_CHAR(OVST.VSTTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS TIME_SERV, 
            BLOODGRP.NAME AS BLOODGRP, 
            OVST.WEIGHT AS WEIGHT, 
            OVST.HEIGHT AS HEIGHT , 
            TO_CHAR(OVST.WEIGHT/((OVST.HEIGHT/100)*(OVST.HEIGHT/100)),'99.99') AS BMI, 
            OVST.BT AS TEMPERATURE, 
            OVST.PR AS PR, 
            OP.RR AS RR, 
            OP.HBPN AS SBP, 
            OP.LBPN AS DBP, 
            NULL AS SYMPTOM, 
            NULL AS DEPCODE, 
            NULL AS DEPARTMENT, 
            NULL AS MOVEMENT_SCORE, 
            NULL AS VOCAL_SCORE, 
            NULL AS EYE_SCORE, OP.O2SAT AS OXYGEN_SAT, NULL AS GAK_COMA_SCO, NULL AS DIAG_TEXT, NULL AS PUPIL_RIGHT, NULL AS PUPIL_LEFT
        FROM RJVT.OVST
        LEFT OUTER JOIN RJVT.PT
            ON OVST.HN = PT.HN 
        LEFT JOIN RJVT.OVSTPRESS OP
            ON OVST.VN = OP.VN AND OVST.HN = OP.HN AND OP.ITEM = 1
        LEFT OUTER JOIN RJVT.BLOODGRP
            ON PT.BLOODGRP = BLOODGRP.BLOODGRP
        WHERE OVST.VN = '${seq}'`);
        return this.propLower(data);
        // return data;
    }

    //15 get physical examination models from his
    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT OPD.VN AS seq, REPLACE( OPD.DIAG, CHR(13) || CHR(10) ) AS pe
        FROM RJVT.CNOPDCARD OPD
        WHERE OPD.VN = '${seq}'`);
        return this.propLower(data);
        // return data;
    }

    //16 get present illness models from his
    async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT OVST.VN AS seq, OVST.CCP AS hpi
        FROM RJVT.OVST
        WHERE OVST.VN = '${seq}'`);
        return this.propLower(data);
        // return data;
    }

    //17 get x-ray models from his
    async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT 
        RVST.VN as seq,  
        TO_CHAR(RVST.RVSTDATE,'YYYY-MM-DD') AS xray_date , RDOEXM.NAME as xray_name 
        FROM RJVT.RVST 
        LEFT OUTER JOIN RJVT.RDOEXM
            ON RVST.RDOGRP = RDOEXM.RDOGRP  
        WHERE RVST.VN = '${seq}'`);
        return this.propLower(data);
        // return data;
    }

    //18 get bed admit models from his
    async getBedsharing(db: Knex) {
        let data = await db.raw(`
        SELECT WARDROOMHST.WARD AS WARD_CODE,
            WARD.DSPNAME AS WARD_NAME,
            COUNT(WARDROOMHST.AN) AS WARD_PT,
            COUNT(WARDROOMHST.WARD) AS WARD_BED,
            NULL AS WARD_STANDARD
        FROM RJVT.WARDROOMHST
        LEFT OUTER JOIN RJVT.WARD ON WARDROOMHST.WARD = WARD.WARD
        WHERE WARDROOMHST.CANCELDATE IS NULL
        AND WARDROOMHST.EXTRABED = 0
        AND WARDROOMHST.WARD not in (-2602 ,995 ,996 ,997 ,998 ,999)
        GROUP BY WARDROOMHST.WARD,
                WARD.DSPNAME
        ORDER BY WARD.DSPNAME`);
        return this.propLower(data);
        // return data;
    }

    //19 get refer out models from his
    async getReferOut(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        SELECT RT.VN AS seq, RT.HN, RT.AN, PN.FULLNAME AS title_name, PT.FNAME AS first_name, PT.LNAME AS last_name, 
        RT.RFRNO AS referno, TO_CHAR(RT.RFRDATE,'YYYY-MM-DD') AS referdate, TO_CHAR(RT.RFRTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS refertime, 
        NULL AS location_name, RT.RFRHPTRECEIVE AS to_hcode, HPT.NAME AS to_hcode_name,
        PT_TYPE.PTTYPE AS pttype_id, PT_TYPE.NAME AS pttype_name,
        NULL AS strength_id, RT.RFRCS AS refer_cause, DCT.LCNO AS doctor, DCT.DSPNAME AS doctor_name
        FROM RJVT.RFRVST RT 
        INNER JOIN RJVT.PT 
            ON PT.HN = RT.HN
        INNER JOIN RJVT.PNAME PN
            ON PN.PNAME = PT.PNAME
        LEFT JOIN RJVT.HPT
            ON HPT.HPT = RT.RFRHPTRECEIVE
        LEFT JOIN RJVT.DCT
            ON DCT.DCT = RT.DCT
        LEFT OUTER JOIN (SELECT INCPRVLG.HN ,INCPTTYPE.MX_DATE ,INCPRVLG.PRVREF ,INCPRVLG.PTTYPE ,PTTYPE.NAME ,INCPRVLG.PRVNO, 
                            INCPRVLG.MAINHPT, HPT1.NAME AS MAINHPT_NAME, INCPRVLG.SUPPHPT, HPT2.NAME AS SUPPHPT_NAME
                            FROM RJVT.INCPRVLG
                            INNER  JOIN (SELECT HN ,MAX(ISSDATE) MX_DATE
                                            FROM RJVT.INCPRVLG 
                                            WHERE CANCELDATE IS NULL
                                            AND SUBTYPE = 10
                                            AND PTTYPEST= 10
                                            GROUP BY HN
                                            ) INCPTTYPE 
                                    ON INCPRVLG.HN = INCPTTYPE.HN AND INCPRVLG.ISSDATE = INCPTTYPE.MX_DATE
                            LEFT OUTER JOIN RJVT.PTTYPE
                                ON INCPRVLG.PTTYPE = PTTYPE.PTTYPE AND PTTYPE.CANCELDATE IS NULL
                            LEFT JOIN RJVT.HPT HPT1
                                ON HPT1.HPT = INCPRVLG.MAINHPT
                            LEFT JOIN RJVT.HPT HPT2
                                ON HPT2.HPT = INCPRVLG.SUPPHPT
                            WHERE INCPRVLG.CANCELDATE IS NULL
                            AND INCPRVLG.SUBTYPE = 10
                            AND INCPRVLG.PTTYPEST= 10) PT_TYPE
            ON RT.HN = PT_TYPE.HN
        WHERE RT.RFRDATE BETWEEN TO_DATE('${start_date}','YYYY-MM-DD') AND TO_DATE('${end_date}','YYYY-MM-DD')
        AND RT.CANCELDATE IS NULL`);
        return this.propLower(data);
        // return data;
    }

    //20 get refer back models from his
    async getReferBack(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        SELECT RT.VN AS seq, RT.HN, NULL AS AN, PN.FULLNAME AS title_name, PT.FNAME AS first_name, PT.LNAME AS last_name, RT.RFRRTNNO AS referno, TO_CHAR(RT.RFRRTNDATE,'YYYY-MM-DD') AS referdate,
        TO_CHAR(RT.RFRRTNTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS refertime, RT.RFRHPTSND AS to_hcode, HPT.NAME AS to_hcode_name, RT.RFRCONTINUE AS refer_cause, DCT.LCNO AS doctor, DCT.DSPNAME AS doctor_name
        FROM RJVT.RFRRETURN RT 
        INNER JOIN RJVT.PT 
            ON PT.HN = RT.HN
        INNER JOIN RJVT.PNAME PN
            ON PN.PNAME = PT.PNAME
        INNER JOIN RJVT.DCT 
            ON DCT.DCT = RT.DCTRTN
        LEFT JOIN RJVT.HPT
            ON HPT.HPT = RT.RFRHPTSND
        WHERE RT.RFRRTNDATE BETWEEN TO_DATE('${start_date}','YYYY-MM-DD') AND TO_DATE('${end_date}','YYYY-MM-DD')
        AND RT.CANCELDATE IS NULL`);
        return this.propLower(data);
        // return data;
    }

    //!21 get refer appoint models from his
    async getAppoint(db: Knex, hn: any, app_date: any) {
        let data = await db.raw(`
        SELECT OA.HN AS seq, TO_CHAR(OA.NEXTDATE,'YYYY-MM-DD') AS  receive_apppoint_date, TO_CHAR(OA.NEXTTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS receive_apppoint_beginttime, 
        TO_CHAR(OA.NEXTTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS receive_apppoint_endtime,
        D.DSPNAME AS receive_apppoint_doctor, L.NAME AS receive_apppoint_chinic, NULL AS receive_text, NULL AS receive_by
        FROM RJVT.OAPP OA 
        LEFT JOIN RJVT.DCT D
            ON D.DCT = OA.DCT
        LEFT JOIN RJVT.CLINICLCT L
            ON OA.NEXTLCT = L.CLINICLCT
        WHERE REFERNO IS NOT NULL
        AND OA.HN = '${hn}'
        AND OA.NEXTDATE = TO_DATE('${app_date}','YYYY-MM-DD')`);
        return this.propLower(data);
        // return data;
    }

    //22 get department or clinic models from his
    async getDepartment(db: Knex) {
        let data = await db.raw(`
        SELECT CLINICLCT.CLINICLCT AS dep_code, CLINICLCT.NAME AS dep_name
        FROM RJVT.CLINICLCT 
        WHERE CLINICLCT.CANCELDATE IS NULL`);
        return this.propLower(data);
        // return data;

    }

    //23 get patient hn from cid models from his
    async getPtHN(db: Knex, cid: any) {
        let data = await db.raw(`
        SELECT PT.HN as hn
        FROM RJVT.PT
        INNER JOIN RJVT.PTNO 
            ON PT.HN = PTNO.HN AND PTNO.NOTYPE IN ('10','20','30') 
        WHERE PTNO.CARDNO = '${cid}'
        `);
        return this.propLower(data);
        // return data;
    }

    //24 get patient medreconcile models from his
    async getMedrecconcile(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT NULL AS hn, NULL AS drug_hospcode , NULL AS drug_hospname, 
        NULL AS drug_name, NULL AS drug_use, NULL AS drug_receive_date 
        FROM DUAL`);
        // return this.propLower(data);
        return [];
    }

    //25 get patient service for coc models from his
    async getServicesCoc(db: Knex, hn: any) {
        let data = await db.raw(`
        WITH smartrefer_visit AS (
            SELECT V.HN, V.VN AS seq, PN.FULLNAME AS title_name, P.FNAME AS first_name, P.LNAME AS last_name, 
                TO_CHAR(V.VSTDATE,'YYYY-MM-DD') AS date_serv, TO_CHAR(V.VSTTIME, 'fm00G00G00','NLS_NUMERIC_CHARACTERS=''.:''') AS time_serv,
                C.NAME AS department, V.NOTE AS detail,  row_number () over (
                       partition by  V.HN
                       order by V.VSTDATE DESC
                     ) RN
            FROM  RJVT.OVST V, RJVT.CLINICLCT C, RJVT.PT P, RJVT.PNAME PN
            WHERE  V.CLINICLCT = C.CLINICLCT 
                AND V.HN = P.HN
                AND PN.PNAME = P.PNAME
                AND V.HN = '${hn}'
            ORDER BY V.VSTDATE DESC
            )
              SELECT HN, SEQ, title_name, first_name, last_name, date_serv, time_serv, department, detail  FROM smartrefer_visit
              WHERE  RN <= 3`);
        return this.propLower(data);
        // return data;
    }

    //26 get patient profile for coc models from his
    async getProfileCoc(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        SELECT PT.HN AS HN,PTNO.CARDNO AS CID, PNAME.FULLNAME AS TITLE_NAME, PT.FNAME AS FIRST_NAME, PT.LNAME AS LAST_NAME, 
        PTADDR.MOO AS MOOPART, PTADDR.ADDR || ' ' || PTADDR.SOI || ' ' || PTADDR.STREET AS ADDRPART, TUMBON.NAME AS TMBPART, 
        AMPUR.NAME AS AMPPART, CHANGWAT.NAME AS CHWPART, TO_CHAR(PT.BRTHDATE,'YYYY-MM-DD') AS BRTHDATE,
        DTOAGE(PT.BRTHDATE ,SYSDATE ,5) AS AGE ,PT_TYPE.PTTYPE AS pttype_id, PT_TYPE.NAME AS pttype_name,PT_TYPE.PRVNO AS pttype_no,
        PT_TYPE.MAINHPT AS hospmain, PT_TYPE.MAINHPT_NAME AS hospmain_name, PT_TYPE.SUPPHPT AS hospsub, PT_TYPE.SUPPHPT_NAME AS hospsub_name, 
        TO_CHAR(PT_TYPE.MX_DATE,'YYYY-MM-DD') AS registdate, TO_CHAR(PT_TYPE.MX_DATE,'YYYY-MM-DD') AS visitdate,
        PTEXT.FTHFNAME ||' ' || PTEXT.FTHLNAME AS father_name, PTEXT.MTHFNAME ||' ' || PTEXT.MTHLNAME AS mother_name, PTEXT.SPSFNAME ||' ' || PTEXT.SPSLNAME AS couple_name, 
        PTINFORMER.INFFNAME || ' ' || PTINFORMER.INFLNAME AS contact_name, PRSNRLT.NAME AS contact_relation, PTINFORMER.MOBILEPHONE AS contact_mobile
        FROM RJVT.PT 
        INNER JOIN RJVT.PTNO 
            ON PTNO.HN = PT.HN AND PTNO.NOTYPE IN ('10','20','30') 
        INNER JOIN RJVT.PNAME 
            ON PNAME.PNAME = PT.PNAME
        LEFT OUTER JOIN RJVT.PTADDR 
            ON PT.HN = PTADDR.HN AND PTADDR.ADDRTYPE = 10 AND PTADDR.ADDRFLAG = 1
        LEFT OUTER JOIN RJVT.TUMBON 
            ON PTADDR.TUMBON = TUMBON.TUMBON AND TUMBON.CANCELDATE IS NULL 
        LEFT OUTER JOIN RJVT.AMPUR 
            ON PTADDR.AMPUR = AMPUR.AMPUR AND AMPUR.CANCELDATE IS NULL 
        LEFT OUTER JOIN RJVT.CHANGWAT 
            ON PTADDR.CHANGWAT = CHANGWAT.CHANGWAT AND CHANGWAT.CANCELDATE IS NULL 
        LEFT OUTER JOIN (SELECT INCPRVLG.HN ,INCPTTYPE.MX_DATE ,INCPRVLG.PRVREF ,INCPRVLG.PTTYPE ,PTTYPE.NAME ,INCPRVLG.PRVNO, INCPRVLG.MAINHPT, HPT1.NAME AS MAINHPT_NAME, INCPRVLG.SUPPHPT, HPT2.NAME AS SUPPHPT_NAME
                        FROM RJVT.INCPRVLG
                        INNER  JOIN (SELECT HN ,MAX(ISSDATE) MX_DATE
                                        FROM RJVT.INCPRVLG 
                                        WHERE HN = '${hn}'
                                        AND CANCELDATE IS NULL
                                        AND SUBTYPE = 10
                                        AND PTTYPEST= 10
                                        GROUP BY HN
                                    ) INCPTTYPE 
                            ON INCPRVLG.HN = INCPTTYPE.HN AND INCPRVLG.ISSDATE = INCPTTYPE.MX_DATE
                        LEFT OUTER JOIN RJVT.PTTYPE
                            ON INCPRVLG.PTTYPE = PTTYPE.PTTYPE AND PTTYPE.CANCELDATE IS NULL
                        LEFT JOIN RJVT.HPT HPT1
                                ON HPT1.HPT = INCPRVLG.MAINHPT
                        LEFT JOIN RJVT.HPT HPT2
                                ON HPT2.HPT = INCPRVLG.SUPPHPT
                        WHERE INCPRVLG.HN = '${hn}'
                        AND INCPRVLG.CANCELDATE IS NULL
                        AND INCPRVLG.SUBTYPE = 10
                        AND INCPRVLG.PTTYPEST= 10) PT_TYPE
                                ON PT.HN = PT_TYPE.HN
        LEFT JOIN RJVT.PTEXT
            ON PTEXT.HN = PT.HN
        LEFT JOIN RJVT.PTINFORMER
            ON PTINFORMER.HN = PT.HN
        LEFT JOIN RJVT.PRSNRLT
            ON PRSNRLT.PRSNRLT = PTINFORMER.PRSNRLT
        WHERE PT.CANCELDATE IS NULL AND PT.HN = '${hn}'`);
        return this.propLower(data);
        // return data;
    }

}
