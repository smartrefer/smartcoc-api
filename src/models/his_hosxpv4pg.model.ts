import Knex = require('knex');
const hospcode = process.env.HIS_CODE;
var md5 = require('md5');

export class HisHosxpv4PgModel {

  async getLogin(db: Knex, username: any, password: any) {
    let pass = md5(password).toUpperCase();
    let data = await db.raw(`
    select o.loginname as username , 
           name as fullname, 
           (select hospitalcode from opdconfig limit 1) as hcode				
    from opduser o		 
    where  o.loginname = '${username}' 		
           and passweb = '${pass}' 
    `);
    return data.rows;
  }

  async getServices(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`
    SELECT
      o.vn AS seq,
      P.pname AS title_name,
      P.fname AS first_name,
      P.lname AS last_name,
      o.vstdate AS date_serv,
      o.vsttime AS time_serv,
      C.department AS department 
    FROM
      ovst AS o
      INNER JOIN kskdepartment AS C ON C.depcode = o.main_dep
      INNER JOIN patient AS P ON P.hn = o.hn 
    WHERE o.hn ='${hn}' and o.vn = '${seq}' UNION
    SELECT
      an.an AS seq,
      P.pname AS title_name,
      P.fname AS first_name,
      P.lname AS last_name,
      i.regdate AS date_serv,
      i.regtime AS time_serv,
      w.NAME AS department 
    FROM
      an_stat AS an
      INNER JOIN ward AS w ON w.ward = an.ward
      INNER JOIN patient AS P ON P.hn = an.hn
      INNER JOIN ipt AS i ON i.an = an.an
    WHERE an.hn ='${hn}' and an.an = '${seq}'
    `);
    return data.rows;
  }

  async getProfile(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`
    SELECT 
      P.hn AS hn,
      P.cid AS cid,
      P.pname AS title_name,
      P.fname AS first_name,
      P.lname AS last_name,
      s.NAME AS sex,
      oc.NAME AS occupation,
      CASE
        WHEN P.moopart IS NULL  OR P.moopart = '' THEN
          '00' 
        WHEN P.moopart = '-' THEN
          '00' 
        WHEN LENGTH ( P.moopart ) = 1 THEN
          concat ( '0', P.moopart ) ELSE P.moopart 
      END AS moopart,
      P.addrpart,
      P.tmbpart,
      P.amppart,
      P.chwpart,
      P.birthday as brthdate,
      REPLACE(REPLACE(REPLACE ( REPLACE ( age( CURRENT_DATE, P.birthday ) :: TEXT, 'years','-') ,'mon','-' ),'days',''),' ','') AS age,
      o.pttype AS pttype_id,
      T.NAME AS pttype_name,
      o.pttypeno AS pttype_no,
      o.hospmain,
      C.NAME AS hospmain_name,
      o.hospsub,
      h.NAME AS hospsub_name,
      P.firstday AS registdate,
      P.last_visit AS visitdate,
      P.fathername AS father_name,
      P.mathername AS mother_name,
      P.informrelation AS contact_name,
      P.informtel AS contact_mobile,
      oc.NAME AS occupation 
    FROM
      patient
      AS P INNER JOIN ovst AS o ON o.hn = P.hn
      LEFT JOIN pttype AS T ON o.pttype = T.pttype
      LEFT JOIN hospcode AS C ON o.hospmain = C.hospcode
      LEFT JOIN hospcode AS h ON o.hospsub = h.hospcode
      LEFT JOIN sex s ON s.code = P.sex
      LEFT JOIN occupation oc ON oc.occupation = P.occupation
    WHERE 
      p.hn = '${hn}' 
    ORDER BY 
      o.vstdate DESC 
    LIMIT 1
    `);
    return data.rows;
  }

  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
      SELECT hospitalcode as provider_code,
             hospitalname as provider_name 
      from opdconfig limit 1
     `);
    return data.rows;
  }

  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT
      agent AS drug_name,
      symptom AS symptom,
      begin_date AS begin_date,
      DATE ( entry_datetime ) AS daterecord 
    FROM
      opd_allergy 
    WHERE hn ='${hn}'
    `);
    return data.rows;
  }

  async getChronic(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT c.icd10 as icd_code,
           cm.regdate as start_date, 
           (CASE WHEN i.tname!='' THEN i.tname ELSE '-' END) as icd_name
    FROM clinicmember as cm 
    inner join clinic c on cm.clinic = c.clinic
    INNER JOIN icd101 as i on i.code = c.icd10
    WHERE cm.hn ='${hn}'
    `);
    return data.rows;
  }

  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT
      o.vn AS seq,
      to_char( o.vstdate :: DATE, 'yyyy-mm-dd' ) AS date_serv,
      to_char( o.vsttime :: TIME, 'HH:mm:ss' ) AS time_serv,
      d.icd10 AS icd_code,
      i.NAME AS icd_name,
      d.diagtype AS diag_type,
      dt.provisional_dx_text AS DiagNote,
      d.diagtype AS diagtype_id 
    FROM
      ovst AS o
      INNER JOIN ovstdiag d ON d.vn = o.vn
      INNER JOIN icd101 AS i ON i.code = d.icd10
      LEFT JOIN ovstdiag_text dt ON o.vn = dt.vn 
    WHERE
      d.vn ='${seq}' 
    UNION
    SELECT
      i.an AS seq,
      to_char( i.dchdate :: DATE, 'yyyy-mm-dd' ) AS date_serv,
      to_char( i.dchtime :: TIME, 'HH:mm:ss' ) AS time_serv,
      ID.icd10 AS icd_code,
      ic.NAME AS icd_name,
      ID.diagtype AS diag_type,
      ic.NAME AS DiagNote,
      ID.diagtype AS diagtype_id 
    FROM
      ipt i
      INNER JOIN iptdiag ID ON ID.an = i.an
      INNER JOIN icd101 ic ON ic.code = ID.icd10 
    WHERE
      i.an = '${seq}'
    `);
    return data.rows;
  }

  async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT
      CASE	
        WHEN LENGTH( r.vn ) >= 9 THEN
          r.vn 
        ELSE 
        o2.vn 
      END AS seq,
      o2.an AS an,
      r.hn AS pid,
      r.hn AS hn,
      REPLACE ( r.refer_number :: TEXT, '/', '-' ) AS referno,
      r.refer_date AS referdate,
      r.refer_hospcode AS to_hcode,
      CASE
        WHEN r.referout_emergency_type_id = '3' THEN
          1 
        ELSE 
          2 
      END AS pttype_id,
      r.refer_cause AS strength_id,
      CASE
        r.refer_cause 
        WHEN 1 THEN
        'Life threatening' 
        WHEN 2 THEN
        'Emergency' 
        WHEN 3 THEN
        'Urgent' 
        WHEN 4 THEN
        'Acute' 
        WHEN 5 THEN
        'Non acute' ELSE'' 
      END AS strength_name,
      r.refer_point AS location_name,
      r.refer_point AS station_name,
      ( SELECT NAME FROM hospcode WHERE hospcode = refer_hospcode ) AS to_hcode_name,
      ( SELECT NAME FROM refer_cause WHERE ID = refer_cause ) AS refer_cause,
      refer_time AS refertime,
      ( SELECT licenseno FROM doctor WHERE code = r.doctor ) AS doctor,
      ( SELECT NAME FROM doctor WHERE code = r.doctor ) AS doctor_name 
    FROM
        referout AS r
        LEFT OUTER JOIN ovst o ON r.vn = o.vn
        LEFT OUTER JOIN ovst o2 ON r.vn = o2.an 
    WHERE
      CASE WHEN LENGTH ( r.vn ) >= 9 THEN r.vn ELSE o2.vn END  = '${seq}'
      AND REPLACE ( r.refer_number :: TEXT, '/', '-' ) = '${referno}' 
    UNION
    SELECT
      CASE WHEN o2.an IS NULL THEN r.vn ELSE o2.an END AS seq,
      o2.an AS an,
      o.hn AS pid,
      o.hn AS hn,
      REPLACE ( r.doc_no :: TEXT, '/', '-' ) AS referno,
      r.reply_date AS referdate,
      r.dest_hospcode AS to_hcode,
      '0' AS pttype_id,
      '0' AS strength_id,
      '' AS strength_name,
      '' AS location_name,
      '' AS station_name,
      ( SELECT NAME FROM hospcode WHERE hospcode = r.dest_hospcode ) AS to_hcode_name,
      '' AS refer_cause,
      r.reply_time AS refertime,
      ( SELECT licenseno FROM doctor WHERE ( code = o.doctor OR code = o2.doctor ) ) AS doctor,
      ( SELECT NAME FROM doctor WHERE ( code = o.doctor OR code = o2.doctor ) ) AS doctor_name 
    FROM
      refer_reply AS r
      LEFT OUTER JOIN ovst o ON r.vn = o.vn
      LEFT OUTER JOIN ovst o2 ON r.vn = o2.an 
    WHERE
      CASE WHEN LENGTH ( r.vn ) >= 9 THEN r.vn ELSE o2.vn END = '${seq}'
      AND REPLACE ( r.doc_no :: TEXT, '/', '-' ) = '${referno}'
    `);
    return data.rows;
  }

  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT A.seq,
      A.date_serv,
      A.time_serv,
      A.drug_name,
      A.qty,
      A.unit,
      A.usage_line1,
      A.usage_line2,
      A.usage_line3 
      FROM
      (
      SELECT
        i.vn AS seq,
        to_char( i.rxdate :: DATE, 'yyyy-mm-dd' ) AS date_serv,
        to_char( i.rxtime :: TIME, 'HH:mm:ss' ) AS time_serv,
        d.NAME AS drug_name,
        SUM ( i.qty ) AS qty,
        d.units AS unit,
        G.name1 AS usage_line1,
        G.name2 AS usage_line2,
        G.name3 AS usage_line3 
      FROM
        ovst o
        INNER JOIN opitemrece AS i ON o.vn = i.vn
        INNER JOIN drugitems AS d ON i.icode = d.icode
        left JOIN drugusage AS G ON G.drugusage = d.drugusage 
        WHERE i.vn = '${seq}'
      GROUP BY
        i.vn,
        i.rxtime,
        d.NAME,
        d.units,
        i.icode,
        i.rxdate,
        G.name1,
        G.name2,
        G.name3 
      UNION
      SELECT
        o.vn AS seq,
        to_char( i.rxdate :: DATE, 'yyyy-mm-dd' ) AS date_serv,
        to_char( i.rxtime :: TIME, 'HH:mm:ss' ) AS time_serv,
        d.NAME AS drug_name,
        SUM ( i.qty ) AS qty,
        d.units AS unit,
        G.name1 AS usage_line1,
        G.name2 AS usage_line2,
        G.name3 AS usage_line3 
      FROM
        ovst o
        INNER JOIN opitemrece AS i ON o.an = i.an
        INNER JOIN drugitems AS d ON i.icode = d.icode
        left JOIN drugusage AS G ON G.drugusage = d.drugusage 
        WHERE i.an = '${seq}'
      GROUP BY
        o.vn,
        i.rxtime,
        d.NAME,
        d.units,
        i.icode,
        i.rxdate,
        G.name1,
        G.name2,
        G.name3 
      ) A 
      WHERE
      A.seq IS NOT NULL
    `);
    return data.rows;
  }

  async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT
      ( SELECT MAX ( receive_date ) FROM lab_head WHERE lab_order_number = lh.lab_order_number ) AS date_serv,
      receive_time AS time_serv,
      form_name AS labgroup,
      lab_items_name_ref AS lab_name,
      lab_order_result AS lab_result,
      li.lab_items_unit AS unit,
      CASE
        WHEN lo.lab_items_normal_value_ref IS NULL THEN
          '' 
        ELSE 
          lo.lab_items_normal_value_ref 
      END AS standard_result 
    FROM
      ovst o
      INNER JOIN lab_head lh ON o.vn = lh.vn 
      OR o.an = lh.vn
      INNER JOIN lab_order lo ON lh.lab_order_number = lo.lab_order_number
      INNER JOIN lab_items li ON li.lab_items_code = lo.lab_items_code 
    WHERE
      lh.vn = '${seq}'  
      AND ( lo.lab_order_result IS NOT NULL AND lo.lab_order_result <> '' )
    `);
    return data.rows;
  }

  async getAppointment(db: Knex, hn: any, dateServ: any, seq: any) {
    let data = await db.raw(`
    SELECT o.vn as seq, 
           o.vstdate as date_serv, 
           o.nextdate as date, 
           o.nexttime as time, 
           (select name from clinic where clinic = o.clinic ) as department, 
           o.app_cause as detail, 
           to_char(ovst.vsttime :: time,'HH:mm:ss') as time_serv
    FROM oapp as o 
    INNER JOIN ovst on ovst.vn = o.vn
    WHERE o.vn ='${seq}'
    `);
    return data.rows;
  }

  async getVaccine(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT o.vstdate as date_serv,
           o.vsttime as time_serv,
           pv.export_vaccine_code as vaccine_code,
           pv.vaccine_name as vaccine_name
    from ovst_vaccine ov
    left outer join  person_vaccine pv on ov.person_vaccine_id = pv.person_vaccine_id     
    LEFT OUTER JOIN  ovst o on o.vn = ov.vn
    WHERE o.hn = '${hn}'
    `);
    return data.rows;
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT
      o.hn AS pid,
      o.vn AS seq,
      o.vstdate AS date_serv,
      o.vsttime AS time_serv,
      od.icd10 AS procedure_code,
      ic.NAME AS procedure_name,
      to_char( od.vstdate :: DATE, 'yyyy-mm-dd' ) AS start_date,
      to_char( od.vsttime :: TIME, 'HH:mm:ss' ) AS start_time,
      to_char( od.vstdate :: DATE, 'yyyy-mm-dd' ) AS end_date,
      '00:00:00' AS end_time 
    FROM
      ovst o
      LEFT OUTER JOIN ovstdiag od ON od.vn = o.vn
      INNER JOIN icd9cm1 ic ON ic.code = od.icd10 
    WHERE 
      o.vn = '${seq}' AND o.an is null
    GROUP BY o.vn,od.icd10,o.hn,o.vstdate,o.vsttime,ic.name,od.vstdate,od.vsttime UNION
    SELECT
      ipt.hn AS pid,
      ipt.an AS seq,
      ipt.regdate AS date_serv,
      ipt.regtime AS time_serv,
      i.icd9 AS procedure_code,
      C.NAME AS procedure_name,
      to_char( i.opdate :: DATE, 'yyyy-mm-dd' ) AS start_date,
      to_char( i.optime :: TIME, 'HH:mm:ss' ) AS start_time,
      to_char( i.enddate :: DATE, 'yyyy-mm-dd' ) AS end_date,
      '00:00:00' AS end_time 
    FROM
      ipt
      INNER JOIN iptoprt i ON i.an = ipt.an
      INNER JOIN icd9cm1 C ON C.code = i.icd9 
    WHERE
      ipt.an = '${seq}'
    GROUP BY ipt.hn,ipt.an,ipt.regdate,ipt.regtime,i.icd9,c.name,i.opdate,i.optime,i.enddate
    `);
    return data.rows;
  }

  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT
      o.vn AS seq,
      to_char( o.vstdate :: DATE, 'yyyy-mm-dd' ) AS date_serv,
      to_char( o.vsttime :: TIME, 'HH:mm:ss' ) AS time_serv,
      P.bloodgrp AS bloodgrp,
      rvs.body_weight_kg,
      CASE WHEN ro.referout_id IS NULL THEN ro2.referout_id ELSE ro.referout_id END AS refer_id,
      CASE WHEN s.bw IS NULL THEN rvs.body_weight_kg ELSE s.bw END AS weight,
      s.height AS height,
      s.bmi AS bmi,
      s.temperature AS temperature,
      s.pulse AS pr,
      s.rr AS rr,
      s.bps AS sbp,
      s.bpd AS dbp,
      CASE
        WHEN ( SELECT cc FROM refer_vital_sign WHERE referout_id = ro.referout_id OR referout_id = ro2.referout_id ) IS NOT NULL THEN
          ( SELECT cc FROM refer_vital_sign WHERE referout_id = ro.referout_id OR referout_id = ro2.referout_id ) 
        ELSE 
          s.cc 
      END AS symptom,
      o.main_dep AS depcode,
      K.department AS department,
      n.gcs_m AS movement_score,
      n.gcs_v AS vocal_score,
      n.gcs_e AS eye_score,
      n.o2sat AS oxygen_sat,
      ( n.gcs_e + n.gcs_v + n.gcs_m ) AS gak_coma_sco,
      '' as diag_text,
      '' AS pupil_right,
      '' AS pupil_left 
    FROM
      ovst AS o
      LEFT JOIN opdscreen AS s ON o.vn = s.vn
      LEFT JOIN referout ro2 ON o.hn = ro2.hn 
      AND o.an = ro2.vn
      LEFT JOIN referout ro ON o.vn = ro.vn
      LEFT JOIN refer_vital_sign rvs ON rvs.referout_id = ro.referout_id
      LEFT JOIN kskdepartment K ON K.depcode = o.main_dep
      INNER JOIN patient AS P ON P.hn = o.hn
      LEFT JOIN er_nursing_detail AS n ON o.vn = n.vn
      LEFT OUTER JOIN ovstdiag_text ON ovstdiag_text.vn = o.vn 
    WHERE
      (o.vn = '${seq}' or ro2.vn ='${seq}') 
    `);
    return data.rows;
  }


  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT
      o.vn AS seq,
      concat (
        o.pe_ga_text,
        o.pe_heent_text,
        o.pe_chest_text,
        o.pe_ab_text,
        o.pe_pv_text,
        o.pe_pr_text,
        o.pe_gen_text,
        o.pe_neuro_text,
        o.pe_ext_text,
        o.pe 
      ) AS pe 
    FROM
      opdscreen_doctor_pe o
    WHERE 
      o.vn = '${seq}'
    `);
    return data.rows;
  }

  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    select DISTINCT ON(refer.hn)
    refer.vn seq,
    refer.hpi hpi
    from referout as refer
    where refer.hn = '${hn}'
    order by refer.hn, refer.refer_date asc
    `);
    return data.rows;
  }

  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT
      x.request_date AS xray_date,
      xr.xray_items_name AS xray_name 
    FROM
      xray_report x
      INNER JOIN xray_items xr ON xr.xray_items_code = x.xray_items_code    
    WHERE 
      x.vn = '${seq}'    
    `);
    return data.rows;
  }


  async getBedsharing(db: Knex) {
    let data = await db.raw(`
    SELECT
      w.ward AS ward_code,
      w.NAME AS ward_name,
      COUNT ( i.an ) AS ward_pt,
      COUNT ( DISTINCT ( b.bedno ) ) AS ward_bed,
      w.bedcount AS ward_standard 
    FROM
      bedno b
      LEFT OUTER JOIN roomno r ON r.roomno = b.roomno
      LEFT OUTER JOIN ward w ON w.ward = r.ward
      LEFT OUTER JOIN iptadm A ON A.bedno = b.bedno
      LEFT OUTER JOIN ipt i ON i.an = A.an 
      AND i.dchdate IS NULL 
    WHERE
      w.ward_active = 'Y' 
    GROUP BY
      w.ward,
      w.NAME,
      w.bedcount
    `);
    return data.rows;
  }

  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT r.vn as seq,r.hn,
           o2.an as an,
           pt.pname as title_name,
           pt.fname as first_name,
           pt.lname as last_name,
           replace(r.refer_number :: text,'/','-') as referno,
           r.refer_date as referdate,
           r.refer_hospcode as to_hcode,
           r.refer_point as location_name,
           (CASE WHEN rt.referout_type_id  = '1' THEN '1' ELSE '2' END) as pttype_id,
					 (CASE WHEN rt.referout_type_id = '1' THEN 'Non Trauma' ELSE 'Trauma' END) as typept_name,
           r.referout_emergency_type_id as strength_id,
           (case r.referout_emergency_type_id
                 when 1 then 'Resucitate'
                 when 2 then 'Emergency'
                 when 3 then 'Urgency'
                 when 4 then 'Semi Urgency'
                 when 5 then 'Non Urgency'
                 else '' end
           ) as strength_name,
           h.name as to_hcode_name,
           rr.name as refer_cause,
           to_char(r.refer_time :: time,'HH:mm:ss') as refertime,
           d.licenseno as doctor,
           d.name as doctor_name
    FROM referout r
    INNER JOIN patient pt on pt.hn = r.hn
    left outer join ovst o2 on r.vn = o2.an
    LEFT OUTER JOIN referout_type rt on rt.referout_type_id = r.referout_type_id
    LEFT OUTER JOIN doctor d on d.code = r.doctor
    LEFT OUTER JOIN rfrcs  rr on rr.rfrcs = r.rfrcs
    LEFT OUTER JOIN hospcode h on h.hospcode = r.refer_hospcode
    WHERE 
    r.refer_number is not null and
    r.referout_type_id in (1,2,3) and
    r.refer_date between '${start_date}' and '${end_date}'
    `);
    return data.rows;
  }

  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT
    ( CASE WHEN o.an IS NULL THEN r.vn ELSE o.an END ) AS seq,
    o.an AS an,
    pt.hn,
    pt.pname AS title_name,
    pt.fname AS first_name,
    pt.lname AS last_name,
    REPLACE ( r.refer_number :: TEXT, '/', '-' ) AS referno,
    r.refer_date AS referdate,
    r.refer_hospcode AS to_hcode,
    h.NAME AS to_hcode_name,
    '' AS refer_cause,
    refer_date AS refertime,
    d.licenseno AS doctor,
    d.NAME AS doctor_name 
    FROM
    referout r
    INNER JOIN ovst o ON o.vn = r.vn or o.an=r.vn
    INNER JOIN patient pt ON pt.hn = o.hn
    LEFT OUTER JOIN hospcode h ON h.hospcode = r.refer_hospcode
    LEFT OUTER JOIN doctor d ON d.code = o.doctor 
    WHERE
    r.refer_number IS NOT NULL 
    AND r.referout_type_id IN ( 4, 5 ,6)
    AND to_char(r.refer_date :: date,'yyyy-mm-dd') between '${start_date}' and '${end_date}'
    `);
    return data.rows;
  }

  async getAppoint(db: Knex, hn: any, app_date: any) {
    let data = await db.raw(`
    SELECT a.vn as seq,
           a.nextdate as receive_apppoint_date,
           a.nexttime as receive_apppoint_beginttime,
           a.endtime as receive_apppoint_endtime,
           a.app_user as receive_apppoint_doctor,
           c.name as receive_apppoint_chinic,
           a.app_cause as receive_text,
           '' as receive_by
    FROM oapp a
    LEFT OUTER JOIN clinic c on c.clinic = a.clinic
    WHERE a.hn = '${hn}' AND a.vstdate = '${app_date}'
    `);
    return data.rows;
  }

  async getDepartment(db: Knex) {
    let data = await db.raw(`
            select depcode as dep_code,department as dep_name from kskdepartment
        `);
    return data.rows;
  }

  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
            select hn from patient where cid = '${cid}'
        `);
    return data.rows;
  }

  async getMedrecconcile(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT
      '' AS drug_hospcode,
      mrd.receive_location AS drug_hospname,
      mrd.medication_name AS drug_name,
      mrd.usage_name as drug_use,
      mrd.last_receive_date as drug_receive_date
    FROM
      med_recon mr
      INNER JOIN med_recon_detail mrd ON mr.med_recon_id = mrd.med_recon_id
    where mr.hn='${hn}'
    `);
    return data.rows;
  }

  async getServicesCoc(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT
      o.hn,o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name,o.vstdate as date_serv,o.vsttime as time_serv,c.department as department
    FROM ovst o
    INNER JOIN kskdepartment c ON c.depcode = o.main_dep
    INNER JOIN patient p ON p.hn = o.hn
    WHERE o.hn ='${hn}'
    UNION
    SELECT
      an.hn,an.an as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name,i.regdate as date_serv,i.regtime as time_serv,w.name as department
    FROM an_stat as an
    INNER JOIN ward as w ON w.ward = an.ward
    INNER JOIN patient as p ON p.hn = an.hn
    INNER JOIN ipt as i On i.an = an.an
    WHERE an.hn ='${hn}'
    ORDER BY seq DESC limit 3
    `);
    return data.rows;
  }

  async getProfileCoc(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT 
      P.hn AS hn,P.cid AS cid,
      P.pname AS title_name,P.fname AS first_name,P.lname AS last_name,
      s.NAME AS sex,oc.NAME AS occupation,
      CASE
        WHEN P.moopart IS NULL  OR P.moopart = '' THEN
          '00' 
        WHEN P.moopart = '-' THEN
          '00' 
        WHEN LENGTH ( P.moopart ) = 1 THEN
          concat ( '0', P.moopart ) ELSE P.moopart 
      END AS moopart,
      P.addrpart,P.tmbpart,P.amppart,P.chwpart,
      P.birthday as brthdate,
      REPLACE(REPLACE(REPLACE ( REPLACE ( age( CURRENT_DATE, P.birthday ) :: TEXT, 'years','-') ,'mons','-' ),'days',''),' ','') AS age,
      o.pttype AS pttype_id,
      T.NAME AS pttype_name,
      o.pttypeno AS pttype_no,
      o.hospmain,C.NAME AS hospmain_name,
      o.hospsub,h.NAME AS hospsub_name,
      P.firstday AS registdate,
      P.last_visit AS visitdate,
      P.fathername AS father_name,
      P.mathername AS mother_name,
      P.informrelation AS contact_name,
      P.informtel AS contact_mobile,
      oc.NAME AS occupation 
    FROM
      patient AS P 
      INNER JOIN ovst AS o ON o.hn = P.hn
      LEFT JOIN pttype AS T ON o.pttype = T.pttype
      LEFT JOIN hospcode AS C ON o.hospmain = C.hospcode
      LEFT JOIN hospcode AS h ON o.hospsub = h.hospcode
      LEFT JOIN sex s ON s.code = P.sex
      LEFT JOIN occupation oc ON oc.occupation = P.occupation
    WHERE 
      p.hn = '${hn}' 
    ORDER BY 
      o.vstdate DESC 
    LIMIT 1
    `);
    return data.rows;
  }

}
