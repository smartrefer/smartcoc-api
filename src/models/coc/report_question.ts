import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_COC_API_URL;

export class ReportQuestionModel {

    async question(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/question`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }
    
      async catagory(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/catagory`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }

      async select_register(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_register`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }

      async select_process_all(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_process_all`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }
      async select_coc(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_coc`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }

      async select_coc_from_hcode(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_coc_from_hcode`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }
      
      async select_send(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_send`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }    
      
      async select_visit(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_visit`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      } 

      async select_regisOne(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_regisOne`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      } 
      
      async select_notify(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_notify`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }       
      async select_get_plan(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_get_plan`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      }       
      async select_get_result(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/select_get_result`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      } 

      async get_result(token:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'POST',
            url: `${urlApi}/question/get_result`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      } 

      async update_referback(token:any, refer_no:any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
          var options = {
            method: 'PUT',
            url: `${urlApi}/question/update_referback/update?refer_no=${refer_no}`,
            agentOptions: {
              rejectUnauthorized: false
            },
            headers:
            {
              'cache-control': 'no-cache',
              'content-type': 'application/json',
              'authorization': `Bearer ${token}`,
            },
            body: { rows: rows },
            json: true
          };
    
          request(options, function (error:any, response:any, body:any) {
            if (error) {
              reject(error);
            } else {
              resolve(body);
            }
          });
        });
      } 

}