import Knex = require('knex');
var md5 = require('md5');
const hospcode = process.env.HIS_CODE;

export class HisHomcModel {
  /* OK */
  async getLogin(db: Knex, username: any, password: any) {
    let pass = md5(password).toUpperCase();
    console.log(pass);
    let data = await db.raw(`
    SELECT RTRIM(LTRIM(profile.UserCode)) as username ,CONCAT(RTRIM(LTRIM(profile.firstName)),' ',RTRIM(LTRIM(profile.lastName))) as fullname, '10669' as hcode 
    from profile WHERE RTRIM(LTRIM(profile.UserCode)) = '${username}' 
    and profile.PassCode = '${pass}' 
    `);
    return data[0];
  }
  /* OK */
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`SELECT OFF_ID as provider_code,rtrim(NAME) as provider_name from HOSPCODE where OFF_ID = '${hospcode}'`);
    return data;
  }
  // ข้อมูลพื้นฐานผู้ป่วย >> OK
  async getProfile(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`SELECT TOP 1 ltrim(rtrim(p.hn)),rtrim(t.titleName) as title_name, rtrim(p.firstName) as first_name, rtrim(p.lastName) as last_name, rtrim(ps.CardID) AS cid
    , p.moo as moopart, (RTRIM(LTRIM(p.addr1)) + '  ' + RTRIM(LTRIM(p.addr2))) as addrpart, p.tambonCode as tmbpart
    , SUBSTRING(p.regionCode, 3, 2) as amppart, p.areaCode as chwpart
    ,convert(date,convert(char, concat(substring(p.birthDay,1,4), case when substring(p.birthDay,5,2)='00' then '01' else substring(p.birthDay,5,2) end, case when substring(p.birthDay,7,2)='00' then '01' else substring(p.birthDay,7,2) end) -5430000)) as brthdate
    ,(case when right(p.birthDay,4) = '0000' then RIGHT('000'+LTRIM(STR(year(getdate())+543 - left(p.birthDay,4))),3)+'-00-00'
    else  RIGHT('000'+LTRIM(STR(year(getdate())+543 - year(p.birthDay))),3) + '-'+ 
    RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthDay, getdate()))%12)),2)+'-'+ 
    RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthDay),DAY(GETDATE())),'-',''))),2) end) as age
    , i.useDrg AS pttype_id,rtrim(pt.pay_typedes) as  pttype_name, RTRIM(LTRIM(ps.SocialID)) as pttype_no
    , i.HMAIN AS hospmain, h1.NAME as hospmain_name, i.HSUB AS hospsub, h2.NAME as hospsub_name
    ,convert(date,convert(char,o.registDate -5430000))  as registDate
    ,(select Top 1 convert(date,convert(char,VisitDate -5430000))  from PATDIAG with(nolock) where Hn=p.hn group by Hn,VisitDate order by VisitDate desc) as VisitDate 
     ,ps.father as father_name 
    ,p.mother as mother_name
    ,'' as couple_name
    ,ps.relatives as contact_name
    ,ps.relationship as contact_relation
    ,'' as contact_mobile
    ,(case when p.sex='ช' then 'ชาย' else 'หญิง' end) as sex
    ,(select occdes from Occup where p.occupation = occcode) as occupation
    FROM PATIENT p
    LEFT OUTER JOIN PTITLE t ON p.titleCode=t.titleCode
    LEFT OUTER JOIN PatSS ps ON p.hn = ps.hn
    LEFT OUTER JOIN OPD_H o ON p.hn = o.hn
    LEFT OUTER JOIN Bill_h i ON o.hn = i.hn AND o.regNo = i.regNo
    LEFT OUTER JOIN Paytype pt ON i.useDrg = pt.pay_typecode 
    LEFT OUTER JOIN DEPT d ON o.dept = d.deptCode
    LEFT OUTER JOIN HOSPCODE h1 ON i.HMAIN = h1.OFF_ID
    LEFT OUTER JOIN HOSPCODE h2 ON i.HSUB = h2.OFF_ID
    WHERE p.hn=dbo.padc('${hn}',' ',7)
    ORDER BY o.registDate DESC`);
    return data[0];
  }

  async getVaccine(db: Knex, hn: any) {
    let data = await db.raw(`select convert(date,convert(char,o.registDate -5430000)) as date_serv,
    CONVERT (time,(left (o.timePt,2)+':'+right (o.timePt,2))) as time_serv,
    p.VACCODE as vaccine_code,v.VACNAME as vaccine_name 
    from PPOP_EPI p
    left join OPD_H o on(o.hn = p.HN and p.REGNO = o.regNo)
    left join PPOP_VACCINE v on(v.VACCODE = p.VACCODE)
    where p.HN = dbo.padc('${hn}',' ',7)`);
    return data;
  }

  async getChronic(db: Knex, hn: any) {
    // let data = await db.raw(`
    // select distinct top 1 rtrim(p.ICDCode) as icd_code,rtrim(ic.DES) as icd_name
    // ,p.VisitDate as 'start_date' 
    // from PATDIAG p with(nolock) 
    // left join OPD_H o with(nolock) on(o.hn = p.Hn and o.regNo = p.regNo) 
    // left join ICD101 ic with(nolock) on(ic.CODE = p.ICDCode) where RTRIM(LTRIM(o.hn))='1309890' and p.DiagType in('I') and p.dxtype = '1' 
    // and ( p.ICDCode between 'I60' and 'I698' or p.ICDCode between 'J45' and 'J46' or p.ICDCode between 'I10' and 'I159' 
    // or p.ICDCode between 'A15' and 'A199' or p.ICDCode between 'E10' and 'E149' or p.ICDCode between 'F30' and 'F399' 
    // or p.ICDCode between 'J43' and 'J449' or p.ICDCode between 'J429' and 'J429' or p.ICDCode between 'I20' and 'I259' 
    // or p.ICDCode between 'I05' and 'I099' or p.ICDCode between 'I26' and 'I289' or p.ICDCode between 'I30' and 'I528' 
    // or p.ICDCode between 'G80' and 'G839' or p.ICDCode between 'D50' and 'D649' or p.ICDCode between 'N17' and 'N19'
    // ) order by p.VisitDate
    // `);
    // return data[0];
  }
  // ประวัติแพ้ยา >> OK
  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`select v.gen_name as drug_name,m.alergyNote as symptom 
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),2)) as begin_date
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),2)) as daterecord
    from medalery m left join Med_inv v on m.medCode=v.abbr where m.hn=dbo.padc('${hn}',' ',7) order by m.updDate desc`);
    return data;
  }
  /* OK */
  async getServices(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(o.hn)) + o.regNo as seq, convert(date,convert(char,o.registDate -5430000)) as date_serv
    ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, d.deptDesc as department 
    from OPD_H as o 
    left join DEPT d on(d.deptCode = o.dept) 
    left join Refer r on o.hn=r.Hn 
    where o.hn = dbo.padc('${hn}',' ',7) and o.regNo =  right('${seq}' ,2)
    union all 
    SELECT i.hn+i.regist_flag as seq
    ,convert(date,convert(char,i.admit_date -5430000)) as date_serv
    ,left(i.admit_time,2)+':'+right(i.admit_time,2) as time_serv
    ,w.ward_name as department
    from Ipd_h as i
    left join Ward w on w.ward_id = i.ward_id
    left join Refer r on i.hn = r.Hn
    where i.hn = dbo.padc('${hn}',' ',7) and i.regist_flag = right('${seq}' ,2)
    `);
    return data[0];
  }
  // การวินิจฉัย  >> OK
  async getDiagnosis(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(p.Hn)) + '-' + p.regNo AS seq
    ,convert(date,convert(char,p.VisitDate -5430000)) AS date_serv
    ,SUBSTRING(o.timePt,1,2)+':'+SUBSTRING(o.timePt,3,4) as time_serv
    ,p.ICDCode as icd_code,ICD101.CODE + '>' + ICD101.DES AS icd_name
    , p.DiagType AS diag_type,isnull(p.DiagNote,'') as DiagNote 
    ,p.dxtype AS diagtype_id
    FROM PATDIAG p
    left join OPD_H o on(o.hn = p.Hn and o.regNo = p.regNo) 
    LEFT JOIN ICD101 ON p.ICDCode = ICD101.CODE 
    WHERE (p.pt_status in('O','Z')) and p.DiagType in('I','E')
    AND o.hn =dbo.padc('${hn}',' ',7) 
    AND p.regNo = right('${vn}' ,2)
    order by p.VisitDate desc`);
    return data;
  }
  /* ข้อมูล Refer >> OK */
  async getRefer(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(`
    select ltrim(rtrim(r.Hn)) + r.RegNo as seq
      , isnull(i.ladmit_n, '') as an
      , ltrim(rtrim(r.Hn)) as pid
      , ltrim(rtrim(r.Hn)) as hn
      , ltrim(rtrim(r.ReferRunno)) as referno
      , convert(date, convert(char, r.ReferDate - 5430000)) as referdate
      , r.ReferHCODE as to_hcode
      , h.NAME as to_hcode_name
      , isnull(rs.REASON_DESC, '') as refer_cause
      , FORMAT(getdate(), N'HH:mm') as refertime
      , '' as doctor
      , '' as doctor_name
    from Refer r
    left join HOSPCODE h on r.ReferHCODE = h.OFF_ID
    left join REFERRS rs on r.ReferReason = rs.REASON_CODE
    left join Ipd_h i on r.Hn = i.hn and r.RegNo = i.regist_flag
    where r.ReferStatus <> 'I' 
    and r.Hn = dbo.padc('${hn}',' ',7) and r.RegNo = right('${vn}', 2) and r.ReferRunno = '${referno}'
    `);
    return data;
  }

  /* OK */
  async getProcedure(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(`select ltrim(rtrim(p.hn)) + o.REGNO as seq,
      (o.OR_DATE) as date_serv, o.OR_TIME as time_serv, o.ORCODE as procedure_code, o.ORDESC as icd_name,
      (o.START_DATE) as start_date, (o.END_DATE) as end_date
    from ORREQ_H o
    left join OPD_H p on(o.HN = p.hn and o.REGNO = p.regNo)
    where o.HN = dbo.padc('${hn}',' ',7) and o.REGNO = right('${vn}', 2) 
    order by date_serv desc `);
    return data;
  }
  /* OK */
  async getDrugs(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(` select x.*
    from (select DISTINCT
    ltrim(rtrim(pm.hn)) + pm.registNo as seq
      , convert(date, convert(char, h.registDate - 5430000)) as date_serv
      , SUBSTRING(h.timePt, 1, 2) + ':' + SUBSTRING(h.timePt, 3, 4) as time_serv
      , m.name as drug_name
      , RTRIM(pm.accAmt) as qty
      , RTRIM(pm.unit) as unit
      , RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1
      , '' as usage_line2
      , '' as usage_line3
    from Patmed pm(NOLOCK)
    left join OPD_H h(NOLOCK) on(h.hn = pm.hn and  h.regNo = pm.registNo)
    left join Med_inv m(NOLOCK)  on(pm.invCode = m.code and m.site = '1')
    where pm.hn = dbo.padc('${hn}',' ',7)
    and h.ipdStatus = '0'
    AND   pm.registNo = right('${vn}', 2)
    union all
    select DISTINCT
    i.hn + i.regist_flag as seq
      , convert(date, convert(char, i.admit_date - 5430000)) as date_serv
      , left(i.admit_time, 2) + ':' + right(i.admit_time, 2) as time_serv
      , m.name as drug_name
      , pm.accAmt as qty
      , m.unit as unit
      , RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1
      , '' as usage_line2
      , '' as usage_line3
    from Patmed pm(NOLOCK)
    left join Ipd_h i(NOLOCK) on(i.hn = pm.hn and  i.regist_flag = pm.registNo)
    left join Med_inv m(NOLOCK)  on(pm.invCode = m.code and m.site = '1')
    where pm.hn = dbo.padc('${hn}',' ',7)  and i.ladmit_n != ''
    AND i.regist_flag = right('${vn}', 2) ) as x 
    order by x.date_serv desc,x.time_serv desc
    `);
    return data;
  }
  /* OK */
  async getLabs(db: Knex, hn: any, dateServe: any, vn: any) {
    let data = await db.raw(` 
    select
    convert(date, convert(char, a.res_date - 5430000)) as date_serv
      , left(a.res_time, 2) + ':' + right(a.res_time, 2) as time_serv
      , isnull(a.lab_code, '') as labgroup
      , a.result_name as lab_name
      , replace(replace(a.real_res,'""',''),'!','') as lab_result
      , s.result_unit as unit
      , a.low_normal + '-' + a.high_normal as standard_result
    from Labres_d a(nolock)
    left join OPD_H o(nolock) on(o.hn = a.hn and o.regNo = a.reg_flag)
    left join Labreq_h b(nolock) on(a.req_no = b.req_no)
    left join Labre_s s(nolock) on(a.lab_code = s.lab_code and a.res_item = s.res_run_no and s.labType = a.lab_type)
    LEFT JOIN Lab lb ON(a.lab_code = lb.labCode and lb.labType = a.lab_type)
    left join Ward w on(w.ward_id = b.reqFrom)
    left join PatSS ss on(ss.hn = a.hn)
    where a.hn = dbo.padc('${hn}',' ',7) AND a.real_res<>''
    AND a.reg_flag = right('${vn}', 2)
    order by a.res_date asc ,left(a.res_time, 2) + ':' + right(a.res_time, 2) desc
    `);
    //console.log(data);

    return data;
  }

  async getAppointment(db: Knex, hn: any, dateServe: any) {
    let data = await db.raw(`select ltrim(rtrim(p.hn)) + p.regNo as seq, a.pre_dept_code as clinic,
      convert(date, convert(char, p.registDate - 5430000)) as date_serv,
      SUBSTRING(p.timePt, 1, 2) + ':' + SUBSTRING(p.timePt, 3, 4) as time_serv,
      convert(date, convert(char, a.appoint_date - 5430000)) as appoint_date,
      a.appoint_time_from as appoint_time, a.appoint_note as detail
    from Appoint a
    left join OPD_H p on(a.hn = p.hn and a.regNo = p.regNo)
    where p.hn = dbo.padc('${hn}',' ',7)`);
    return data;
  }

  /*ข้อมูลการรักษา >> OK*/
  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
   select top 1 ltrim(rtrim(v.hn)) + v.RegNo as seq
    ,convert(date, convert(char, v.VisitDate)) as date_serv
    ,CONVERT(VARCHAR(5),v.VisitDate, 108)  AS time_serv
    ,(select PATIENT.bloodGroup from PATIENT where PATIENT.bloodGroup <> '' and PATIENT.hn = v.hn) as bloodgrp
    ,v.Weight AS weight, v.Height as height
    ,v.BMI as bmi
    ,v.Temperature AS temperature
    ,v.Pulse AS pr
    ,v.Breathe AS rr
    ,v.Hbloodpress as sbp
    ,v.Lbloodpress AS dbp
    ,v.Symtom as symptom
    ,v.deptCode as depcode
    ,(select deptDesc from DEPT d where d.deptCode = v.deptCode) as department
    ,v.GCS_M as movement_score
    ,v.GCS_V as vocal_score
    ,v.GCS_E as eye_score
    ,v.O2sat as oxygen_sat
    ,v.pupil_right as pupil_right
    ,v.pupil_left as pupil_left
    ,CONVERT(INT, v.GCS_M)+CONVERT(INT, v.GCS_V)+CONVERT(INT, v.GCS_E) as gak_coma_sco
    ,(select top 1 ltrim(rtrim(dx2.DiagNote)) from PATDIAG dx2 where dx2.Hn = v.hn and dx2.regNo=v.RegNo and dx2.dxtype='1' and dx2.DiagType='I') as diag_text
    from VitalSign v
    left join PATDIAG dx on(v.hn=dx.Hn and v.RegNo=dx.regNo)
    where v.hn= dbo.padc('${hn}',' ',7) and v.RegNo = right('${seq}', 2)
    order by v.VitalSignNo desc
      `);
    return data;
  }
  /* ข้อมูล PE >> OK */
  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq
    , '' as pe
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc

      `);
    return data;
  }
  /* ข้อมูล HPI >> OK */
  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`SELECT
    ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq
      , '' as hpi
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc

      `);
    return data;
  }
  async getBedsharing(db: Knex) {
    let data = await db.raw(`    
SELECT ward_id as ward_code,
case
when w.ward_id = '121' then 'gen1'
when w.ward_id = '125' then 'gen2'
when w.ward_id = '123' then 'gen3'
when w.ward_id = '124' then 'gen4'
when w.ward_id = '131' then 'ศัลย์ทางเดินปัสสาวะ'
when w.ward_id = 's11' then 'CVT'
when w.ward_id = '122' then 'Trauma'
when w.ward_id = '541' then 'Chest'
when w.ward_id = 'w55' then 'Neuro หญิง'
when w.ward_id = 'w43' then 'Neuro ชาย'
when w.ward_id = 'w51' then 'ศัลย์เด็ก'
when w.ward_id = 'w53' then 'ศัลย์ตกแต่ง'
when w.ward_id = '211' then 'สูติกรรม 1'
when w.ward_id = '221' then 'สูติกรรม 2'
when w.ward_id = '132' then 'สูติกรรม 3'
when w.ward_id = '231' then 'นรีเวชกรรม'
when w.ward_id = '241' then 'Ortho ชาย 1'
when w.ward_id = '142' then 'Ortho ชาย 2'
when w.ward_id = '242' then 'Ortho หญิง'
when w.ward_id = '144' then 'Spinal Unit'
when w.ward_id = '331' then 'อช.ชั้น 4'
when w.ward_id = '341' then 'อช.ชั้น 5'
when w.ward_id = '862' then 'อช.ชั้น 6'
when w.ward_id = '841' then 'อญชั้น 4'
when w.ward_id = '232' then 'อญชั้น 5'
when w.ward_id = '321' then 'อญชั้น 6'
when w.ward_id = 'chm' then 'เคมีบำบัด'
when w.ward_id = '344' then 'อายุรกรรมโรคเลือด'
when w.ward_id in ('441','443') then 'เด็ก 1,2'
when w.ward_id = '421' then 'เด็ก 3'
when w.ward_id = '431' then 'เด็ก 4'
when w.ward_id = '332' then 'เด็ก 5'
when w.ward_id = '621' then 'จักษุ'
when w.ward_id = '342' then 'โสต ศอ นาสิก'
when w.ward_id = '641' then 'พิเศษอายุรกรรม7ทิศใต้'
when w.ward_id = '651' then 'พิเศษอายุรกรรม7ทิศเหนือ'
when w.ward_id = '711' then 'พิเศษพระปทุมฯ1'
when w.ward_id = '721' then 'พ.ปทุมฯ2,BMT'
when w.ward_id = '731' then 'พิเศษพระปทุมฯ3'
when w.ward_id = '741' then 'พิเศษพระปทุมฯ4'
when w.ward_id = '751' then 'พิเศษพระปทุมฯ5'
when w.ward_id = 'b2r' then 'พิเศษประกันสังคม'
when w.ward_id = 'w61' then 'พิเศษศัลยกรรม'
when w.ward_id = 's61' then 'พิเศษสงฆ์'
when w.ward_id = 'w62' then 'พิเศษวิชิต'
when w.ward_id = '112' then 'ห้องคลอด'
end as ward_name,
(select count(i.hn) from Ipd_h i where ward_id=w.ward_id and i.discharge_status='0') as ward_pt,
w.ward_tot_bed as ward_bed,
w.ward_tot_bed as ward_standard
from Ward w where (ward_tot_bed<>'' and ward_tot_bed<>'99') and (ward_id<>'b2r' and ward_id<>'211' and ward_id<>'obs')
and w.ward_id <> 'w91'
and w.ward_id <> '543'
and w.ward_id <> 'gp'
and w.ward_id <> 'p01'
and w.ward_id <> 'w11'
and w.ward_id not in ('111','114','115','143','141','113','s41','w41','w45','w47','b2l','411','222','311','333','323','343','224','s31','s32','322','423','424','212','214','215')
    `);
    return data;
  }

  /* OK */
  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    select ltrim(rtrim(r.Hn)) + r.RegNo as seq
      , r.Hn as hn
      , isnull(i.ladmit_n, '') as an
      , rtrim(t.titleName) as title_name
      , rtrim(p.firstName) as first_name
      , rtrim(p.lastName) as last_name
      , r.ReferRunno as referno
      , convert(date, convert(char, r.ReferDate - 5430000)) as referdate
      , r.ReferHCODE as to_hcode
      , h.NAME as to_hcode_name
      , isnull(rs.REASON_DESC, '') as refer_cause
      , FORMAT(getdate(), N'hh:mm') as refertime
      , '' as doctor
      , '' as doctor_name
    from Refer r
    left join PATIENT p on r.Hn = p.hn
    LEFT JOIN PTITLE t ON p.titleCode = t.titleCode
    left join HOSPCODE h on r.ReferHCODE = h.OFF_ID
    left join REFERRS rs on r.ReferReason = rs.REASON_CODE
    left join Ipd_h i on r.Hn = i.hn and r.RegNo = i.regist_flag
    where r.ReferStatus <> 'I' and convert(char, r.ReferDate - 5430000)  between replace('${start_date}', '-', '') 
    and replace('${end_date}', '-', '') and r.ReferHCODE<>'10669'`);
    return data;
  }
  /* OK */
  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT
    ltrim(rtrim(Refer.Hn)) + Refer.RegNo as seq
      , Refer.Hn as hn
      , isnull(i.ladmit_n, '') as an
      , RTRIM(PTITLE.titleName) as title_name
      , rtrim(PATIENT.firstName) as first_name
      , rtrim(PATIENT.lastName) as last_name
      , Refer.ReferRunno AS referno
        , convert(date, convert(char, Refer.ReferDate - 5430000)) as referdate
        , Refer.ReferHCODE AS to_hcode
          , HOSPCODE.NAME as to_hcode_name
          , isnull(rs.REASON_DESC, '') as refer_cause
      , FORMAT(getdate(), N'hh:mm') as refertime
          , '' AS doctor
            , '' as doctor_name
    FROM Refer with (nolock)
    INNER JOIN PATIENT with (nolock) ON Refer.Hn = PATIENT.hn
    INNER JOIN PTITLE with (nolock) ON PATIENT.titleCode = PTITLE.titleCode
    LEFT JOIN HOSPCODE with (nolock) ON Refer.ReferHCODE = HOSPCODE.OFF_ID
    left join REFERRS rs on Refer.ReferReason = rs.REASON_CODE
    left join Ipd_h i on Refer.Hn = i.hn and Refer.RegNo = i.regist_flag
    WHERE   convert(char, Refer.ReferDate - 5430000)  BETWEEN replace('${start_date}', '-', '') and replace('${end_date}', '-', '')
    AND Refer.ReferReason in (6,7,8,9,10) `);
    return data;
  }

  async getAppoint(db: Knex, hn: any, app_date: any) {
    let data = await db.raw(`
    SELECT a.hn as seq
    ,convert(date,convert(char,a.appoint_date -5430000)) as receive_appoint_date
    ,a.appoint_time_from as receive_appoing_begintime
    ,a.appoint_time_to as receive_appoing_endtime
    ,dbo.TitleFullName(d.docName,d.docLName) as receive_appoing_doctor
    ,ltrim(rtrim(deptDesc)) as receive_appoing_chinic
    ,appoint_note as receive_text
    ,(RTRIM(pr.firstName)+' '+RTRIM(pr.lastName)) as receive_by 
    ,a.remark as receive_special_note
    ,concat(ltrim(rtrim(deptDesc)),'(',dep.deptCode,')') as receive_checkpoint
    ,concat('045-319200',' ต่อ ',teldept) as receive_teldept
    FROM Appoint a left join PATIENT p on p.hn=a.hn
    left join PTITLE pti on pti.titleCode=p.titleCode
    left join DOCC d on d.docCode=a.doctor
    left join DEPT dep on dep.deptCode=a.pre_dept_code
    left join profile pr on pr.UserCode=a.maker
    WHERE
    (a.app_type = 'A')
    and convert(char, a.appoint_date - 5430000)  = replace('${app_date}', '-', '')
    and a.hn='${hn}' order by a.keyin_time desc
        `);
    return data;
  }
  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
          select convert(date, convert(char, x.xreq_date - 5430000)) as xray_date,c.xproc_des as xray_name
          from XresHis x 
          left join OPD_H o (NOLOCK) on (o.hn = x.hn and o.regNo = x.regist_flag) 
          LEFT JOIN Xreq_h xh on(xh.hn = x.hn and xh.regist_flag = x.regist_flag and x.xrunno = xh.xrunno) 
          LEFT JOIN Xpart p ON(x.xpart_code = p.xpart_code) 
          LEFT JOIN Xreq_d d ON(x.xprefix = d.xprefix AND x.xrunno = d.xrunno) 
          LEFT JOIN Xcon xc on(x.xprefix = xc.xprefix) 
          LEFT JOIN Xproc c ON(d.xpart_code = c.xproc_part  AND d.xproc_code = c.xproc_code) 
          WHERE xc.xchr_code <> 'PAT' 
          AND x.hn = dbo.padc('${hn}',' ',7)
          AND x.regist_flag = right('${seq}', 2)
          order by  x.xreq_date DESC
         `);
    return data;
  }

  async getDepartment(db: Knex) {
    let data = await db.raw(`
      SELECT ltrim(rtrim(ward_id)) as dep_code,ltrim(rtrim(ward_name)) as dep_name from Ward order by ward_name asc
    `);
    return data
  }
  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
      select hn from PatSS with(nolock) where CardID = '${cid}'
    `);
    return data[0];
  }

  async getMedrecconcile(db: Knex, hn: any) {
    let data = await db.raw(`
        select '' as drug_hospcode ,'' as drug_hospname,'' as drug_name,'' as drug_use,'' as drug_receive_date
        `);
    return data;
  }

    /* OK */
    async getServicesCoc(db: Knex, hn: any) {
      let data = await db.raw(`
      SELECT top 3 o.hn, ltrim(rtrim(o.hn)) + o.regNo as seq, convert(date,convert(char,o.registDate -5430000)) as date_serv
      ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, d.deptDesc as department 
      from OPD_H as o 
      left join DEPT d on(d.deptCode = o.dept) 
      left join Refer r on o.hn=r.Hn 
      where o.hn = dbo.padc('${hn}',' ',7)
      GROUP BY o.hn,o.regNo,o.registDate,o.timePt,d.deptDesc
      order by seq desc
      `);
      return data;
    }

    async getProfileCoc(db: Knex, hn: any) {
      let data = await db.raw(`
      SELECT TOP 1 ltrim(rtrim(p.hn)),rtrim(t.titleName) as title_name, rtrim(p.firstName) as first_name, rtrim(p.lastName) as last_name, rtrim(ps.CardID) AS cid
      , p.moo as moopart, (RTRIM(LTRIM(p.addr1)) + '  ' + RTRIM(LTRIM(p.addr2))) as addrpart, p.tambonCode as tmbpart
      , SUBSTRING(p.regionCode, 3, 2) as amppart, p.areaCode as chwpart
      ,(case when right(p.birthDay,4) = '0000' then RIGHT('000'+LTRIM(STR(year(getdate())+543 - left(p.birthDay,4))),3)+'-00-00'
      else  RIGHT('000'+LTRIM(STR(year(getdate())+543 - year(p.birthDay))),3) + '-'+ 
      RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthDay, getdate()))%12)),2)+'-'+ 
      RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthDay),DAY(GETDATE())),'-',''))),2) end) as age 
      , i.useDrg AS pttype_id,rtrim(pt.pay_typedes) as  pttype_name, RTRIM(LTRIM(ps.SocialID)) as pttype_no
      , i.HMAIN AS hospmain, h1.NAME as hospmain_name, i.HSUB AS hospsub, h2.NAME as hospsub_name
      ,convert(date,convert(char,o.registDate -5430000))  as registDate
      ,convert(date,convert(char,p.birthDay -5430000)) as dob
      ,(select Top 1 convert(date,convert(char,VisitDate -5430000))  from PATDIAG with(nolock) where Hn=p.hn group by Hn,VisitDate order by VisitDate desc) as VisitDate 
       ,ps.father as father_name 
      ,p.mother as mother_name
      ,'' as couple_name
      ,ps.relatives as contact_name
      ,ps.relationship as contact_relation
      ,'' as contact_mobile
      ,(case when p.sex='ช' then 'ชาย' else 'หญิง' end) as sex
      ,(select occdes from Occup where p.occupation = occcode) as occupation
      FROM PATIENT p
      LEFT OUTER JOIN PTITLE t ON p.titleCode=t.titleCode
      LEFT OUTER JOIN PatSS ps ON p.hn = ps.hn
      LEFT OUTER JOIN OPD_H o ON p.hn = o.hn
      LEFT OUTER JOIN Bill_h i ON o.hn = i.hn AND o.regNo = i.regNo
      LEFT OUTER JOIN Paytype pt ON i.useDrg = pt.pay_typecode 
      LEFT OUTER JOIN DEPT d ON o.dept = d.deptCode
      LEFT OUTER JOIN HOSPCODE h1 ON i.HMAIN = h1.OFF_ID
      LEFT OUTER JOIN HOSPCODE h2 ON i.HSUB = h2.OFF_ID
      WHERE p.hn=dbo.padc('${hn}',' ',7)
      ORDER BY o.registDate DESC
      `);
      return data[0];
    }
}
