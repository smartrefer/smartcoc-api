FROM keymetrics/pm2:14-alpine

LABEL maintainer="Thawatchai Seangduan <thawatchai.sea2@gmail.com>"

WORKDIR /home/api

RUN apk update && apk upgrade && apk add tzdata && apk add --no-cache alpine-sdk git \
  alpine-sdk \
  openssh \
  autoconf \
  # python \
  build-base \
  libtool \
  bash \
  automake \
  g++ \
  make \
  && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
  && echo 'Asia/Bangkok' > /etc/timezone

COPY . .

RUN npm i && npm run build

RUN rm -rf src

CMD ["pm2-runtime", "process.json"]
